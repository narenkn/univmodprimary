#include <string>
#include <iostream>
#include <random>
#include <gtest/gtest.h>

#include "nlohmann/json.hpp"
#include "top/genmain.h"
#include "top/main.h"
#include "numbertheory.h"

void
maths_c11p_c6_g1_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Eleven-Plus",
    "chapter": "Ch-6",
    "id": "G-1",
    "enabled": true,
    "title": "Divisibility rules",
    "controller": "Maths::NumberTheory-DivisibilityRules",
    "view": "ViewsMultiinputsBtnchoice",
    "test": "BtnChoiceViewTest",
    "table_for": [
      99
    ],
    "test_type": "random",
    "num_vars": 5,
    "min_digits": 4,
    "max_digits": 5,
    "div_min_digits": 0,
    "div_max_digits": 1,
    "ans_optional": false
  }
)", NumberTheoryTest, maths_c11p_c6_g1_t1);

void
maths_c11p_c35_jj1_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Eleven-Plus",
    "chapter": "Ch-35",
    "id": "JJ-1",
    "enabled": true,
    "title": "Divisibility rule (x2) skill builders",
    "controller": "Maths::NumberTheory-DivisibilityRules",
    "view": "ViewsMultiinputsBtnchoice",
    "test": "BtnChoiceViewTest",
    "table_for": [
      2
    ],
    "test_type": "YesNo",
    "num_vars": 5,
    "min_digits": 4,
    "max_digits": 5,
    "div_min_digits": 0,
    "div_max_digits": 1,
    "ans_optional": false
  }
)", NumberTheoryTest, maths_c11p_c35_jj1_t1);

void
maths_c4_c0_a10_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Class-4",
    "chapter": "Ch-0",
    "id": "A-10",
    "enabled": true,
    "title": "Mark integers on number line",
    "controller": "Maths::NumberTheory-NumberLine",
    "view": "ViewsMultiinputsD3simpleSequence",
    "test": "D3OnlyViewTest",
    "minval": -20,
    "maxval": 20
  }
)", NumberTheoryTest, maths_c4_c0_a10_t1);

void
maths_c11p_c0_a13_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Eleven-Plus",
    "chapter": "Ch-0",
    "id": "A-13",
    "enabled": false,
    "title": "Roman numerals upto 100",
    "controller": "Maths::NumberTheory-ToRoman",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "minval": 1,
    "maxval": 100
  }
)", NumberTheoryTest, maths_c11p_c0_a13_t1);

void
maths_c3_c0_a4_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Class-3",
    "chapter": "Ch-0",
    "id": "A-4",
    "enabled": true,
    "title": "Find the missing number in the sequence",
    "controller": "Maths::NumberTheory-SkipCount",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "skipby": [ 2, 10 ],
    "num_vars": 6,
    "start_digits": 1
  }
)", NumberTheoryTest, maths_c3_c0_a4_t1);
