#include <string>
#include <iostream>
#include <random>
#include <gtest/gtest.h>

#include "nlohmann/json.hpp"
#include "top/genmain.h"
#include "top/main.h"
#include "twodim.h"

void
maths_c3_c16_q10_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Class-3",
    "chapter": "Ch-16",
    "id": "Q-10",
    "enabled": true,
    "title": "Find the area of rectangles and squares",
    "controller": "Maths::Twodim-AreaPerimeter",
    "view": "ViewsMultiinputsD3shapes:ViewsMultiinputsInput",
    "test": "D3shapesViewTest:MultiInputsViewTest",
    "test_type": "area",
    "ticks": 30,
    "num_edges": 7,
    "units": "1cm"
  }
)", TwodimTest, maths_c3_c16_q10_t1);

void
maths_c3_c16_q10_t2(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Class-4",
    "chapter": "Ch-13",
    "id": "N-3",
    "enabled": true,
    "title": "Perimeter of rectilinear shapes",
    "controller": "Maths::Twodim-AreaPerimeter",
    "view": "ViewsMultiinputsD3shapes:ViewsMultiinputsInput",
    "test": "D3shapesViewTest:MultiInputsViewTest",
    "test_type": "perimeter",
    "ticks": 30,
    "num_edges": 7,
    "units": "1cm"
  }
)", TwodimTest, maths_c3_c16_q10_t2);
