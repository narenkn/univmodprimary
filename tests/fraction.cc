#include <string>
#include <iostream>
#include <random>
#include <gtest/gtest.h>

#include "nlohmann/json.hpp"
#include "top/genmain.h"
#include "top/main.h"
#include "fraction.h"

void
maths_c11p_c11_l4_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Eleven-Plus",
    "chapter": "Ch-11",
    "id": "L-4",
    "enabled": true,
    "title": "Equivalent fractions",
    "ask": "",
    "controller": "Maths::Fraction-Equivalent",
    "view": "ViewsMultiinputsBtnchoice",
    "test": "BtnChoiceViewTest",
    "ans_optional": false,
    "num_vars": 4,
    "digits": 1,
    "multiplier_digits": 1
  }
)", FractionTest, maths_c11p_c11_l4_t1);

void
maths_c11p_c11_l6_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
  auto n{stoi(retj["correct_ans"][0][1].get<std::string>())};
  auto d{stoi(retj["correct_ans"][0][3].get<std::string>())};
  if (n > 1)
    ASSERT_NE(0, d%n) << "n:" << n << " d:" << d << ", " << retj.dump();
}
TEST_F_WRAP(R"(
  {
    "class": "Eleven-Plus",
    "chapter": "Ch-11",
    "id": "L-6",
    "enabled": true,
    "title": "Write fractions in lowest terms",
    "ask": "",
    "controller": "Maths::Fraction-LowestTerms",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "num_vars": 2,
    "digits": 1,
    "test_type": "simple",
    "multiplier_digits": 1
  }
)", FractionTest, maths_c11p_c11_l6_t1);

void
maths_c11p_c12_m5_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Eleven-Plus",
    "chapter": "Ch-12",
    "id": "M-5",
    "enabled": false,
    "title": "Add three or more fractions with the same denominator",
    "ask": "",
    "controller": "Maths::Fraction-Operate",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "op_types": [ "+" ],
    "num_vars": 2,
    "digits": 1,
    "multiplier_digits": 1,
    "multiply_whole": false,
    "like_fraction": true
  }
)", FractionTest, maths_c11p_c12_m5_t1);

void
maths_c11p_c13_n8_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Eleven-Plus",
    "chapter": "Ch-13",
    "id": "N-8",
    "enabled": false,
    "title": "Multiply fractions by whole numbers",
    "ask": "",
    "controller": "Maths::Fraction-Operate",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "num_vars": 1,
    "op_types": [
      "*"
    ],
    "like_fraction": true,
    "multiply_whole": true,
    "multiplier_digits": 1,
    "digits": 1
  }
)", FractionTest, maths_c11p_c13_n8_t1);

void
maths_c11p_c13_n24_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Eleven-Plus",
    "chapter": "Ch-13",
    "id": "N-24",
    "enabled": false,
    "title": "Divide fractions by whole numbers",
    "ask": "",
    "controller": "Maths::Fraction-Operate",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "num_vars": 1,
    "op_types": [
      "/"
    ],
    "like_fraction": true,
    "multiply_whole": true,
    "multiplier_digits": 1,
    "digits": 1
  }
)", FractionTest, maths_c11p_c13_n24_t1);

void
maths_c11p_c12_m11_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Eleven-Plus",
    "chapter": "Ch-12",
    "id": "M-11",
    "enabled": false,
    "title": "Add fractions with different denominators, express in lowest terms:",
    "ask": "",
    "controller": "Maths::Fraction-Operate",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "op_types": [ "+" ],
    "num_vars": 2,
    "digits": 1,
    "multiplier_digits": 1,
    "multiply_whole": false,
    "like_fraction": false
  }
)", FractionTest, maths_c11p_c12_m11_t1);

void
maths_c11p_c12_m13_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Eleven-Plus",
    "chapter": "Ch-12",
    "id": "M-13",
    "enabled": false,
    "title": "Subtract fractions with different denominators, express in lowest terms:",
    "ask": "",
    "controller": "Maths::Fraction-Operate",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "op_types": [ "-" ],
    "num_vars": 2,
    "digits": 1,
    "multiplier_digits": 1,
    "multiply_whole": false,
    "like_fraction": false
  }
)", FractionTest, maths_c11p_c12_m13_t1);

void
maths_c11p_c12_m14_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Eleven-Plus",
    "chapter": "Ch-12",
    "id": "M-14",
    "enabled": false,
    "title": "Add and subtract fractions with different denominators",
    "ask": "",
    "controller": "Maths::Fraction-Operate",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "op_types": [ "+", "-" ],
    "num_vars": 2,
    "digits": 1,
    "multiplier_digits": 1,
    "multiply_whole": false,
    "like_fraction": false
  }
)", FractionTest, maths_c11p_c12_m14_t1);

void
maths_c11p_c12_m3_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Eleven-Plus",
    "chapter": "Ch-12",
    "id": "M-3",
    "enabled": false,
    "title": "Add and subtract fractions with the same denominator",
    "ask": "",
    "controller": "Maths::Fraction-Operate",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "op_types": [ "+", "-" ],
    "num_vars": 2,
    "digits": 1,
    "multiplier_digits": 1,
    "multiply_whole": false,
    "like_fraction": true
  }
)", FractionTest, maths_c11p_c12_m3_t1);

void
maths_c11p_c11_l21_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
  auto n{retj["datastr"]["os"][0]["x2"].get<uint32_t>()}, d{retj["datastr"]["os"][0]["x2"].get<uint32_t>()};
  ASSERT_NE(0, n) << "n:" << n << retj.dump();
  ASSERT_NE(0, d) << "d:" << d << retj.dump();
}
TEST_F_WRAP(R"(
  {
    "class": "Eleven-Plus",
    "chapter": "Ch-11",
    "id": "L-21",
    "enabled": true,
    "title": "What fraction is the smallest line compared with the longer one?",
    "controller": "Maths::Fraction-Illustrate",
    "view": "ViewsMultiinputsD3shapes:ViewsMultiinputsInput",
    "test": "ArraySelectViewTest:MultiInputsViewTest",
    "num_vars": 1,
    "digits": 2,
    "maxlen": 30
  }
)", FractionTest, maths_c11p_c11_l21_t1);

void
maths_c11p_c11_l22_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
  auto n{retj["datastr"]["op1"].get<uint32_t>()}, d{retj["datastr"]["op2"].get<uint32_t>()};
  auto slen{retj["datastr"]["sequences"][0]["values"].size()};
  ASSERT_LT(n, d) << "n:" << n << " d:" << d << " ret:" << retj.dump();
  ASSERT_GT(slen, d) << retj.dump();
}
TEST_F_WRAP(R"(
  {
    "class": "Eleven-Plus",
    "chapter": "Ch-11",
    "id": "L-22",
    "enabled": true,
    "title": "Mark fraction on number lines",
    "controller": "Maths::Fraction-MarkOnNumberLines",
    "view": "ViewsMultiinputsD3simpleSequence",
    "test": "D3OnlyViewTest",
    "is_decimal": false,
    "minval": 10,
    "maxval": 20
  }
)", FractionTest, maths_c11p_c11_l22_t1);

void
maths_c11p_c11_l23_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Eleven-Plus",
    "chapter": "Ch-11",
    "id": "L-23",
    "enabled": true,
    "title": "Write value of expression in lowest terms",
    "controller": "Maths::Fraction-LowestTerms",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "num_vars": 2,
    "digits": 1,
    "test_type": "1minus",
    "multiplier_digits": 1
  }
)", FractionTest, maths_c11p_c11_l23_t1);

void
maths_c11p_c8_i15_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
  auto n{retj["datastr"]["op1"].get<uint32_t>()}, d{retj["datastr"]["op2"].get<uint32_t>()};
  auto slen{retj["datastr"]["sequences"][0]["values"].size()};
  ASSERT_LT(n, d) << "n:" << n << " d:" << d << " ret:" << retj.dump();
  ASSERT_GT(slen, d) << retj.dump();
}
TEST_F_WRAP(R"(
  {
    "class": "Eleven-Plus",
    "chapter": "Ch-11",
    "id": "L-22",
    "enabled": true,
    "title": "Mark fraction on number lines",
    "controller": "Maths::Fraction-MarkOnNumberLines",
    "view": "ViewsMultiinputsD3simpleSequence",
    "test": "D3OnlyViewTest",
    "is_decimal": true,
    "minval": 0,
    "maxval": 10
  }
)", FractionTest, maths_c11p_c8_i15_t1);

void
maths_c4_c14_o9_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Class-4",
    "chapter": "Ch-14",
    "id": "O-9",
    "enabled": true,
    "title": "Fraction of a number",
    "controller": "Maths::Fraction-LowestTerms",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "num_vars": 2,
    "digits": 1,
    "test_type": "fractionof",
    "multiplier_digits": 1
  }
)", FractionTest, maths_c4_c14_o9_t1);

void
maths_c4_c14_o10_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Class-4",
    "chapter": "Ch-14",
    "id": "O-10",
    "enabled": true,
    "title": "Convert fractions to decimals",
    "controller": "Maths::Fraction-LowestTerms",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "num_vars": 2,
    "digits": 1,
    "test_type": "convert2dec",
    "multiplier_digits": 1
  }
)", FractionTest, maths_c4_c14_o10_t1);

void
maths_c4_c14_o11_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Class-4",
    "chapter": "Ch-14",
    "id": "O-11",
    "enabled": true,
    "title": "Convert decimals to fractions",
    "controller": "Maths::Fraction-LowestTerms",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "num_vars": 2,
    "digits": 1,
    "test_type": "convertdec2frac",
    "multiplier_digits": 1
  }
)", FractionTest, maths_c4_c14_o11_t1);

void
maths_c4_c14_o12_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Class-4",
    "chapter": "Ch-14",
    "id": "O-12",
    "enabled": true,
    "title": "Convert fraction to percent",
    "controller": "Maths::Fraction-LowestTerms",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "num_vars": 2,
    "digits": 1,
    "test_type": "convert2pcent",
    "multiplier_digits": 1
  }
)", FractionTest, maths_c4_c14_o12_t1);

void
maths_c11p_cz_a28_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
  ASSERT_EQ(retj.contains("ints"), true);
  ASSERT_EQ(retj.contains("nfract"), true);
  ASSERT_EQ(retj["ints"].is_array(), true);
  ASSERT_EQ(retj["fracts"].is_array(), true);

  auto nint = confj["nint"].get<uint32_t>();
  auto nfract = confj["nfract"].get<uint32_t>();
  ASSERT_EQ(retj["ints"].size(), nint);
  ASSERT_EQ(retj["fracts"].size(), nfract);
  for (auto ui=0; ui<nint; ui++) {
    ASSERT_EQ(retj["ints"][ui].is_number(), true);
  }
  for (auto ui=0; ui<nfract; ui++) {
    ASSERT_EQ(retj["fracts"][ui].is_array(), true);
    ASSERT_EQ(retj["fracts"][ui][0].is_number(), true);
    ASSERT_EQ(retj["fracts"][ui][1].is_number(), true);
  }
}
TEST_F_WRAP(R"(
  {
    "class": "Eleven-Plus",
    "chapter": "Ch-Z",
    "id": "A-28",
    "enabled": false,
    "title": "Generic Test Generator",
    "controller": "Maths::Fraction-GenericTestRandom",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "nint": 10,
    "nfract": 10,
    "digits": 1
  }
)", FractionTest, maths_c11p_cz_a28_t1);

