#include <string>
#include <iostream>
#include <random>
#include <gtest/gtest.h>

#include "nlohmann/json.hpp"
#include "top/genmain.h"
#include "top/main.h"
#include "percent.h"

void
maths_c11p_c19_t8_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Eleven-Plus",
    "chapter": "Ch-19",
    "id": "T-8",
    "enabled": false,
    "title": "Percent of a number",
    "ask": "",
    "controller": "Maths::Percent-Number",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "num_vars": 1,
    "test_type": "ask",
    "digits": 2
  }
)", PercentTest, maths_c11p_c19_t8_t1);

void
maths_c11p_c19_t1_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
  uint32_t tcount{0}, tot{0};
  for (auto &v: retj["datastr"]["fill"]) {
    tot++;
    if (v.get<bool>()) tcount++;
  }
  ASSERT_EQ(0, ((tcount * 100) % tot)) << "tcount:" << tcount << " tot:" << tot << retj.dump();
  ASSERT_GT(tcount, 0);
}
TEST_F_WRAP(R"(
  {
    "class": "Eleven-Plus",
    "chapter": "Ch-19",
    "id": "T-1",
    "enabled": true,
    "title": "What percentage is illustrated?",
    "ask": "",
    "controller": "Maths::Percent-Number",
    "view": "ViewsMultiinputsD3arrayselect:ViewsMultiinputsInput",
    "test": "ArraySelectViewTest:MultiInputsViewTest",
    "num_vars": 1,
    "test_type": "illustrate",
    "digits": 2
  }
)", PercentTest, maths_c11p_c19_t1_t1);

void
maths_c11p_c11_l20_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
  uint32_t tcount{0}, tot{0}, sq;
  for (auto &v: retj["datastr"]["fill"]) {
    tot++;
    if (v.get<bool>()) tcount++;
  }
  sq = retj["datastr"]["size"].get<uint32_t>();
  ASSERT_EQ(tot, sq*sq);
  ASSERT_EQ(tot, std::stoi(retj["correct_ans"][0][3].get<std::string>()));
  ASSERT_EQ(tcount, std::stoi(retj["correct_ans"][0][1].get<std::string>()));
  ASSERT_GT(tcount, 0);
}
TEST_F_WRAP(R"(
  {
    "class": "Eleven-Plus",
    "chapter": "Ch-11",
    "id": "L-20",
    "enabled": true,
    "title": "What fraction is illustrated $(N/D)$ (ignore grey)?",
    "ask": "",
    "controller": "Maths::Percent-Number",
    "view": "ViewsMultiinputsD3arrayselect:ViewsMultiinputsInput",
    "test": "ArraySelectViewTest:MultiInputsViewTest",
    "num_vars": 1,
    "test_type": "illustrate-fraction",
    "digits": 2
  }
)", PercentTest, maths_c11p_c11_l20_t1);
