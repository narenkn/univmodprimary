#include <string>
#include <iostream>
#include <random>
#include <gtest/gtest.h>

#include "nlohmann/json.hpp"
#include "top/genmain.h"
#include "top/main.h"
#include "decimal.h"

void
maths_c11p_c7_h6_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Eleven-Plus",
    "chapter": "Ch-7",
    "id": "H-6",
    "enabled": true,
    "title": "Convert decimals between standard and expanded form",
    "ask": "",
    "controller": "Maths::Decimal-ConvertStandardExpanded",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "min_digits": 1,
    "max_digits": 3
  }
)", DecimalTest, maths_c11p_c7_h6_t1);

void
maths_c3_c1_b8_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Class-3",
    "chapter": "Ch-1",
    "id": "B-8",
    "enabled": true,
    "title": "Convert between standard and expanded form",
    "controller": "Maths::Decimal-ConvertStandardExpanded",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "min_digits": 1,
    "max_digits": 1
  }
)", DecimalTest, maths_c3_c1_b8_t1);

void
maths_c3_c19_t1_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Class-3",
    "chapter": "Ch-19",
    "id": "T-1",
    "enabled": true,
    "title": "Rounding to nearest 1 or 10 or 100",
    "controller": "Maths::Decimal-Rounding",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "min_digits": 1,
    "max_digits": 4,
    "rounding": "near",
    "roundto": [10, 100]
  }
)", DecimalTest, maths_c3_c19_t1_t1);

void
maths_c11p_c8_i16_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Eleven-Plus",
    "chapter": "Ch-8",
    "id": "I-16",
    "enabled": true,
    "title": "Multiply/ Divide decimal numbers by powers of 10",
    "controller": "Maths::Decimal-OperateBy10",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "num_vars": 2,
    "op_types": [
      "*", "/"
    ],
    "min_digits": 2,
    "max_digits": 3
  }
)", DecimalTest, maths_c11p_c8_i16_t1);

void
maths_c4_c0_a4_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Class-4",
    "chapter": "Ch-0",
    "id": "A-4",
    "enabled": true,
    "title": "Ordinal numbers to 100th",
    "controller": "Maths::Decimal-Ordinal",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "maxval": 100
  }
)", DecimalTest, maths_c4_c0_a4_t1);

