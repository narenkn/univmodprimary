#include <string>
#include <iostream>
#include <random>
#include <gtest/gtest.h>

#include "nlohmann/json.hpp"
#include "top/genmain.h"
#include "top/main.h"
#include "placevalue.h"

void
maths_c11p_c7_h4_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Eleven-Plus",
    "chapter": "Ch-7",
    "id": "H-4",
    "enabled": true,
    "title": "Place values in decimal numbers",
    "controller": "Maths::PlaceValue-FindDigit",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "min_digits": 2,
    "max_digits": 3,
    "test_type": "placevalue"
  }
)", PlaceValueTest, maths_c11p_c7_h4_t1);

void
maths_c4_c0_a11_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Class-4",
    "chapter": "Ch-0",
    "id": "A-11",
    "enabled": true,
    "title": "Find digit in place values",
    "controller": "Maths::PlaceValue-FindDigit",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "min_digits": 2,
    "max_digits": 3,
    "test_type": "finddigit"
  }
)", PlaceValueTest, maths_c4_c0_a11_t1);

