#include <string>
#include <iostream>
#include <random>
#include <gtest/gtest.h>

#include "nlohmann/json.hpp"
#include "top/genmain.h"
#include "top/main.h"
#include "currency.h"

void
maths_c3_c13_n8_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Class-3",
    "chapter": "Ch-13",
    "id": "N-8",
    "enabled": true,
    "title": "Add money amounts - word problems",
    "controller": "Maths::Currency-Coins",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "test_type": "howmanycoins",
    "denom_pence": [1, 2, 5, 10, 20, 50],
    "denom_pound": [1, 2],
    "pound2pence": 100,
    "pencename": "pence",
    "poundname": "pound",
    "pencestr": "p",
    "poundstr": "£",
    "min_coins": 0,
    "max_coins": 5
  }
)", CurrencyTest, maths_c3_c13_n8_t1);

void
maths_c3_c13_n10_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Class-3",
    "chapter": "Ch-13",
    "id": "N-10",
    "enabled": true,
    "title": "How much pence to reach next pound?",
    "controller": "Maths::Currency-Coins",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "test_type": "howmanymorepence",
    "denom_pence": [1, 2, 5, 10, 20, 50],
    "denom_pound": [1, 2],
    "pound2pence": 100,
    "pencename": "pence",
    "poundname": "pound",
    "pencestr": "p",
    "poundstr": "£",
    "min_coins": 0,
    "max_coins": 5
  }
)", CurrencyTest, maths_c3_c13_n10_t1);

void
maths_c3_c13_n4_t1(nlohmann::json& confj, nlohmann::json& retj, bool debug)
{
}
TEST_F_WRAP(R"(
  {
    "class": "Class-3",
    "chapter": "Ch-13",
    "id": "N-4",
    "enabled": true,
    "title": "Making change",
    "controller": "Maths::Currency-Coins",
    "view": "ViewsMultiinputsInput",
    "test": "MultiInputsViewTest",
    "test_type": "makeamount",
    "denom_pence": [1, 2, 5, 10, 20, 50],
    "denom_pound": [1, 2],
    "pound2pence": 100,
    "pencename": "pence",
    "poundname": "pound",
    "pencestr": "p",
    "poundstr": "£",
    "min_coins": 0,
    "max_coins": 5,
    "num_coins": 3
  }
)", CurrencyTest, maths_c3_c13_n4_t1);
