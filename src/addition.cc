#include <random>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include "nlohmann/json.hpp"
#ifdef SABUILD
#include "sa/genmain.h"
#else
# ifdef WASMBUILD
  #include "src/genmain.h"
# else
  #include "top/genmain.h"
# endif
#endif
#include "addition.h"

namespace {

  struct optreeState {
    bool is_missingin, is_missingval, is_missingop, is_findx,
      is_findx2, is_findx2x, is_negints;
    optreeState():
      is_missingin{false}, is_missingval{false}, is_missingop{false},
      is_findx{false}, is_findx2{false}, is_findx2x{false}, is_negints{false} {}
  };

  void
  str2htabledata(const uint32_t& cols, std::vector<std::string>& in, nlohmann::json& ret)
  {
    ret["th"] = nlohmann::json::array();
    ret["htabledata"] = nlohmann::json::array();
    ret["correct_ans"] = nlohmann::json::array();

    /* first push null to all locations */
    // for (auto &ins: in) std::cout << "\"" << ins << "\"" << "\n";
    for (auto j = 0; j<cols; j++) {
      ret["th"][j] = nullptr;
    }

    for (auto hi=in.begin(); hi!=in.end(); hi+=2) {
      ret["htabledata"].push_back(nlohmann::json::array());
      ret["correct_ans"].push_back(nlohmann::json::array());
      for (auto ui=0; ui<cols; ui++) {
        ret["htabledata"].back().push_back((ui < hi->length()) ? hi->substr(ui,1) : "");
      }
      auto hi1 = hi+1;
      for (auto ui=0; ui<cols; ui++) {
        if ((ui >= hi1->length()) || ('#'==(*hi1)[ui])) ret["correct_ans"].back().push_back(nullptr);
        else if (' '==(*hi1)[ui]) ret["correct_ans"].back().push_back("");
        else ret["correct_ans"].back().push_back(hi1->substr(ui,1));
      }
    }
  }

  class OpTree {
  public:
    const uint32_t id;
    uint32_t selid;
    std::unique_ptr<OpTree> l, r;
    std::string op;
    int32_t v, lv, rv;
    OpTree(uint32_t& _id, const uint32_t num_vars, uint32_t& _selid):
      id{_id}, v{0}, lv{0}, rv{0} {
      auto nlhs = (num_vars+1)/2, nrhs = (num_vars-nlhs);
      _id += 2;
      if (nlhs > 1) {
        if (id == _selid) _selid = _id;
        l = std::make_unique<OpTree>(_id, nlhs, _selid);
      }
      if (nrhs > 1) {
        if ((id+1) == _selid) _selid = _id;
        r = std::make_unique<OpTree>(_id, nrhs, _selid);
      }
      selid = _selid;
    }
    int32_t
    randomize(std::shared_ptr<RandGenerator> rand, nlohmann::json& config, optreeState& state) {
      auto min_num = config["min_num"].get<uint32_t>();
      auto max_num = config["max_num"].get<uint32_t>();
      int32_t xval = config.contains("xval") ? config["xval"].get<uint32_t>() : 0;
      do {
        auto i = rand->get() % config["op_types"].size();
        op = config["op_types"][i].get<std::string>();
      } while (false);
      for (uint32_t ui=0; ui<10; ui++) {
        rv = r ? r->randomize(rand, config, state) :
          (int32_t) ((0 != xval) && ((id+1) == selid) ? xval :
                     rand->get_range(min_num, max_num));
        if (state.is_negints) {
          if (! r)
            rv = rand->get_range(0,1) & 1 ? -rv : rv;
        } else if (rv > 0) break;
      }
      for (uint32_t ui=0; ui<10; ui++) {
        lv = l ? l->randomize(rand, config, state) :
          (int32_t) ((0 != xval) && (id == selid) ? xval :
                     rand->get_range(min_num, max_num));
        if (state.is_negints) {
          if (! l)
            lv = rand->get_range(0,1) & 1 ? -lv : lv;
        } else if (lv > 0) break;
      }
      if ("+" == op) v = lv+rv;
      else if ("-" == op) v = lv-rv;
      else if ("*" == op) v = lv*rv;
      else {
        handle_divby(rv);
        v = lv / rv;
      }

      return v;
    }
    void
    handle_divby(int32_t tv) /* result should be mul by this value  */
    {
      if (l) l->handle_divby(tv);
      lv *= tv;
      if (("+" == op) or ("-" == op)) {
        if (r) r->handle_divby(tv);
        rv *= tv;
      }
      v *= tv;
    }
    std::string
    get_sel_ans(bool is_missingin=false, bool is_missingop=false, bool is_findx=false)
    {
      auto _is_missingop = is_missingop && (id == (selid&~1));
      auto _is_missingin = (is_missingin || is_findx) && (id == (selid&~1));
      if (_is_missingop) return op;
      else if (is_missingop)
        return (l ? l->get_sel_ans(is_missingin, is_missingop, is_findx) : "") +
          (r ? r->get_sel_ans(is_missingin, is_missingop, is_findx) : "");
      /* should be is_missingin */
      return (l ? l->get_sel_ans(is_missingin, is_missingop, is_findx) : (_is_missingin && (id == selid) ? std::to_string(lv) : "")) +
        (r ? r->get_sel_ans(is_missingin, is_missingop, is_findx) : (_is_missingin && ((id+1) == selid) ? std::to_string(rv) : ""));
    }
    std::string
    to_tex(bool addbrac=true, bool is_missingin=false, bool is_missingop=false, bool is_findx=false) {
      auto _is_missingop = is_missingop && (id == (selid&~1));
      auto _is_missingin = (is_missingin || is_findx) && (id == (selid&~1));
      // std::cout << "is_findx:" << (is_findx ? "true" : "false") << " _is_missingin:" << (_is_missingin ? "true" : "false") << " id:" << id << " selid:" << selid << std::endl;
      auto lstr = (_is_missingin && (id == selid)) ? ((is_findx) ? "x" : "?") : (l ? l->to_tex(true, is_missingin, is_missingop, is_findx) : std::to_string(lv));
      auto rstr = (_is_missingin && ((id+1)==selid)) ? ((is_findx) ? "x" : "?") : (r ? r->to_tex(true, is_missingin, is_missingop, is_findx) : std::to_string(rv));
      auto opstr = op;
      if ("/" == op) {
        lstr = "\\frac{" + lstr + "}";
        rstr = "{" + rstr + "}";
        opstr = "";
      } else if ("*" == op) {
        opstr = " \\times ";
      }
      if (_is_missingop) opstr = "\\ ? \\ ";
      return (addbrac ? "(" : "") + lstr + opstr + rstr + (addbrac ? ")" : "");
    }
  };

  class ThreeOrMoreMissing : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

  class MultiplicationSentenceForNumberLines : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

  class ArraysModelMultiplication : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans);
  };

  class CountEqual : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

  class IdentifyExprMseg : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

  class Tables : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

  class AdditionPatternIncreasingPlacevalues : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

  class DragGroup : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

  class Expression : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

  class RatioProblem : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

}

Maths::Addition::Addition():
  GenMod(__CLASS_NAME__)
{
  MAKE_CONTROLLER(ThreeOrMoreMissing);
  MAKE_CONTROLLER(MultiplicationSentenceForNumberLines);
  MAKE_CONTROLLER(ArraysModelMultiplication);
  MAKE_CONTROLLER(CountEqual);
  MAKE_CONTROLLER(IdentifyExprMseg);
  MAKE_CONTROLLER(Tables);
  MAKE_CONTROLLER(AdditionPatternIncreasingPlacevalues);
  MAKE_CONTROLLER(DragGroup);
  MAKE_CONTROLLER(Expression);
  MAKE_CONTROLLER(RatioProblem);
}

void
ThreeOrMoreMissing::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  bool is_missing_digits = config["is_missing_digits"].get<bool>(); /* not used ... */
  bool is_missing_inputs = is_missing_digits || config["is_missing_inputs"].get<bool>();
  bool is_decimal = config["is_decimal"].get<bool>();

  /* choose op */
  std::string op;
  do {
    auto i = rand->get() % config["op_types"].size();
    op = config["op_types"][i].get<std::string>();
  } while (false);

  /* static conf */
  ret["text"] = std::string("Complete the **") + ((op == "+") ? "Addition" : (("*" == op) ? "Multiplication" : (("/" == op) ? "Division" : "Subtraction"))) + "** table:";
  ret["inboxwidth"] = "50px";

  /* first generate the values */
  auto num_vars = config.contains("num_vars") ?
    config["num_vars"].get<uint32_t>() : rand->get_range(3, 6);
  auto vars = std::vector<uint32_t>();
  auto svars = std::vector<std::string>();
  auto min_digits = config["min_digits"].get<uint32_t>();
  auto max_digits = config["max_digits"].get<uint32_t>();
  auto decloc {rand->get_range(0, min_digits)};
  auto cols = 0;
  uint32_t ca = 0, cm = 1;
  for (auto i=0; i<num_vars-1; i++) {
    vars.emplace_back(rand->get_digits_no0(min_digits, max_digits));
    ca += vars.back();
    cm *= vars.back();
    svars.emplace_back(std::to_string(vars[i]));
    cols = svars[i].length() > cols ? svars[i].length() : cols;
  }
  do {
    auto d = rand->get_digits_no0(min_digits, max_digits);
    if (op == "+") {
      vars.emplace_back(d);
      ca += d;
      svars.emplace_back(std::to_string(d));
      cols = svars.back().length() > cols ? svars.back().length() : cols;
    } else if (op == "*") {
      vars.emplace_back(d);
      ca = cm * d;
      svars.emplace_back(std::to_string(d));
      cols = svars.back().length() > cols ? svars.back().length() : cols;
    } else if (op == "/") {
      cm *= d;
      ca = d;
      vars.insert(vars.begin(), cm);
      svars.insert(svars.begin(), std::to_string(cm));
      cols = svars.front().length() > cols ? svars.front().length() : cols;
    } else /* op == "-" */ {
      vars.insert(vars.begin(), d+ca);
      svars.insert(svars.begin(), std::to_string(d+ca));
      cols = svars[0].length() > cols ? svars[0].length() : cols;
      ca = d;
    }
  } while (false);
  auto sca = std::to_string(ca);
  cols = sca.length() > cols ? sca.length() : cols;
  cols++;

  /* first get strings for all tables */
  const uint32_t user_input_row = is_missing_inputs ? rand->get_range(0, num_vars) : num_vars;
  std::vector<std::string> htd;
  {
    for (auto i=0; i<=num_vars; i++) {
      std::stringstream ss, sa;
      if (user_input_row == i) {
        ss << std::setw(cols) << std::setfill('_') << "";
        sa << std::setw(cols) << std::setfill(' ') << ((i>=num_vars) ? sca : svars[i]);
      } else {
        ss << std::setw(cols) << std::setfill(' ') << ((i>=num_vars) ? sca : svars[i]);
        sa << std::setw(cols) << std::setfill('#') << "";
      }
      htd.emplace_back(ss.str());
      htd.emplace_back(sa.str());
    }
  }

  /* */
  if (is_decimal) {
    if (0 == decloc) decloc++;
    /* add 2 columns */
    cols += 2;
    auto ui=0;
    for (auto hi=htd.begin(); hi!=htd.end(); hi+=2, ui++) {
      auto hi1 = hi+1;
      hi->insert(1, 1, hi->at(0));
      hi1->insert(1, 1, hi1->at(0));
      (*hi) += (ui == user_input_row) ? "_" : "0";
      (*hi1) += "0";
      auto tdecloc = ((hi1+1) != htd.end()) ? decloc :
        ("*"==op) ? (decloc<<1) : ("/"==op) ? 0 : /* +- */ decloc;
      hi->insert(cols-tdecloc-1, 1, (ui == user_input_row) ? '_' : '.');
      if (std::string::npos != hi->find(" .")) hi->replace(hi->find(" ."), 2, "0.");
      hi1->insert(cols-tdecloc-1, 1, '.');
      if ( (ui == user_input_row) && (std::string::npos != hi1->find(" .")) )
        hi1->replace(hi1->find(" ."), 2, "0.");
    }
  }

  /* */
  ret["var0"] = vars[0]; ret["var1"] = vars[1]; ret["ca"] = ca; ret["decloc"] = decloc;  /* debug */
  str2htabledata(cols, htd, ret);
  ret["htabledata"][num_vars-1][0] = ("/"==op) ? "&divide;" : ("*"==op) ? "&times;" : op;
}

void
MultiplicationSentenceForNumberLines::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  /* choose op */
  std::string op;
  do {
    auto i = rand->get() % config["op_types"].size();
    op = config["op_types"][i].get<std::string>();
  } while (false);
  bool add_from0 {op == "add-from0"};

  /* */
  auto min_digits = config["min_digits"].get<uint32_t>();
  auto max_digits = config["max_digits"].get<uint32_t>();

  auto op1 = rand->get_digits(min_digits, max_digits) + 1; /* distance */
  auto op2 = rand->get_digits(min_digits, max_digits) + 1; /* num-seq */
  auto op3 = op2;
  auto start = add_from0 ? 0 : rand->get_digits(min_digits, max_digits);
  op3 = ((op3+10)/10)*10 + 1; /* make is to next 10 */

  /* question */
  auto op1_blank = (rand->get() & 1) ? true : false;
  auto ans_blank = (rand->get() & 1) ? true : false;
  ret["text"] = "Complete the multiplication sentence that describes the model:";
  ret["textinput"] = nlohmann::json::array();
  ret["textinput"][0] = nlohmann::json::array();
  ret["correct_ans"] = nlohmann::json::array();
  ret["correct_ans"][0] = nlohmann::json::array();
  ret["textinput"][0].push_back(op1_blank ? "_" : std::to_string(op1));
  if (op1_blank) ret["correct_ans"][0].push_back(std::to_string(op1));
  else ret["correct_ans"][0].push_back(nullptr);
  ret["textinput"][0].push_back(" &times; ");
  ret["correct_ans"][0].push_back(nullptr);
  ret["textinput"][0].push_back(op1_blank ? std::to_string(op2) : "_");
  if (!op1_blank) ret["correct_ans"][0].push_back(std::to_string(op2));
  else ret["correct_ans"][0].push_back(nullptr);
  ret["textinput"][0].push_back(" = ");
  ret["correct_ans"][0].push_back(nullptr);
  ret["textinput"][0].push_back(ans_blank ? "_" : std::to_string(op1*op2));
  if (ans_blank) ret["correct_ans"][0].push_back(std::to_string(op1*op2));
  else ret["correct_ans"][0].push_back(nullptr);

  /* fill others */
  ret["datastr"] = nlohmann::json::object();
  ret["datastr"]["height"] = 30;
  ret["datastr"]["width"] = 300;

  /* make the sequence */
  ret["datastr"]["sequences"] = nlohmann::json::array();
  ret["datastr"]["sequences"].push_back(nlohmann::json::object());
  ret["datastr"]["sequences"][0]["values"] = nlohmann::json::array();
  ret["datastr"]["sequences"][0]["arcs"] = nlohmann::json::array();
  do {
    auto i = rand->get() % Genmain::htmlColors.size();
    ret["datastr"]["sequences"][0]["color"] = Genmain::htmlColors[i];
  } while (false);
  for (auto i3=0; i3<op3; i3++) {
    ret["datastr"]["sequences"][0]["values"].push_back(i3*op1);
    ret["datastr"]["sequences"][0]["arcs"].push_back(bool((i3>=0) && ((i3*op1)<(op1*op2))));
  }
}

void
ArraysModelMultiplication::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  /* init */
  auto min_digits = config["min_digits"].get<uint32_t>();
  auto max_digits = config["max_digits"].get<uint32_t>();

  auto op1 = rand->get_digits(min_digits, max_digits) + 1; /* no 0's */
  auto op2 = rand->get_digits(min_digits, max_digits) + 1; /* no 0's */
  auto ans = op1 * op2;

  ret["text"] = "Make rectangular array of squares to model : $" + std::to_string(op1) + " \\times " + std::to_string(op2) + " = " + std::to_string(ans) + "$";
  ret["datastr"] = nlohmann::json::object();
  ret["datastr"]["size"] = 15;
  ret["correct_ans"] = nlohmann::json::array();
  for (auto i=15; i; i--)
    for (auto j=15; j; j--)
      ret["correct_ans"].push_back(bool((i<=op1) && (j<=op2)));
  ret["op1"] = op1;
  ret["op2"] = op2;
  ret["ans"] = ans;
}

void
ArraysModelMultiplication::check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans)
{
  uint32_t nsel{0};
  uint32_t low_x{15}, low_y{15}, high_x{0}, high_y{0};
  ret["result"] = false;

  /* first count should be equal */
  if (!ans.is_array()) return;
  if (ans.size() != 15*15) return;
  for (uint32_t i=0; i<ans.size(); i++) {
    auto &v = ans[i];
    if (!v.is_boolean()) return;
    if (v.get<bool>()) {
      uint32_t x{i/15}, y{i%15};
      if (x < low_x) low_x = x;
      if (y < low_y) low_y = y;
      if (x > high_x) high_x = x;
      if (y > high_y) high_y = y;
      nsel++;
    }
  }
  if (nsel != question["ans"].get<uint32_t>()) return;

  /* check for rectangle */
  auto op1{question["op1"].get<uint32_t>()}, op2{question["op2"].get<uint32_t>()};
  op1--, op2--;
  if (((low_x + op1) != high_x) && ((low_x + op2) != high_x)) return;
  if (((low_y + op1) != high_y) && ((low_y + op2) != high_y)) return;

  /* all checks passed... */
  ret["result"] = true;
}

void
CountEqual::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  std::string fcol, bkcol, symbol;
  const std::string is {config["is"].get<std::string>()};

  /* choose op */
  std::string op;
  do {
    auto i = rand->get() % config["op_types"].size();
    op = config["op_types"][i].get<std::string>();
  } while (false);

  /* init */
  auto min_digits = config["min_digits"].get<uint32_t>();
  auto max_digits = config["max_digits"].get<uint32_t>();
  do {
    auto i = rand->get() % Genmain::htmlColors.size();
    fcol = Genmain::htmlColors[i];
  } while (false);
  do {
    auto i = rand->get() % Genmain::htmlBackgroundColors.size();
    bkcol = Genmain::htmlBackgroundColors[i];
  } while (false);
  do {
    auto i = rand->get() % Genmain::symbols.size();
    symbol = Genmain::symbols[i];
  } while (false);

  /* */
  ret["text"] = "Fill blanks to describe the model:";
  ret["draw_symbols"] = nlohmann::json::array();

  /* generate the test */
  auto op1 = rand->get_digits(min_digits, max_digits) + 1; /* no 0's */
  auto op2 = rand->get_digits(min_digits, max_digits) + 1;
  auto ans = op1 * op2;
  for (auto i=op2; i; i--) {
    auto o = nlohmann::json::object();
    o["rows"] = nlohmann::json::array();
    o["height"] = 100;
    o["width"] = 400;
    o["box"] = true;
    o["cols"] = 3;
    o["box_color"] = bkcol;
    o["rows"].push_back(nlohmann::json::object
                        ({
                          {"symbol", symbol},
                          {"num", op1},
                          {"color", fcol},
                        })
                        );
    ret["draw_symbols"].push_back(o);
  }
  auto op1_blank = (rand->get() & 1) ? true : false;
  auto op2_blank = (rand->get() & 1) ? true : false;
  auto ans_blank = ((!op1_blank) && (!op2_blank)) ? true :
    (rand->get() & 1) ? true : false;

  /* */
  ret["textinput"] = nlohmann::json::array();
  ret["correct_ans"] = nlohmann::json::array();
  ret["textinput"][0] = nlohmann::json::array();
  ret["correct_ans"][0] = nlohmann::json::array();
  if (is == "CountEqualGroups") {
    ret["textinput"][1] = nlohmann::json::array();
    ret["correct_ans"][1] = nlohmann::json::array();
    if (op1_blank || op2_blank) {
      ret["textinput"][0].push_back("There are ");
      ret["correct_ans"][0].push_back(nullptr);
      if (op1_blank) {
        ret["textinput"][0].push_back("_");
        ret["correct_ans"][0].push_back(std::to_string(op1));
      } else {
        ret["textinput"][0].push_back("*"+std::to_string(op1)+"*");
        ret["correct_ans"][0].push_back(nullptr);
      }
      ret["textinput"][0].push_back(" groups of ");
      ret["correct_ans"][0].push_back(nullptr);
      if (op2_blank) {
        ret["textinput"][0].push_back("_");
        ret["correct_ans"][0].push_back(std::to_string(op2));
      } else {
        ret["textinput"][0].push_back("*"+std::to_string(op2)+"*");
        ret["correct_ans"][0].push_back(nullptr);
      }
      ret["textinput"][0].push_back(" symbols.");
      ret["correct_ans"][0].push_back(nullptr);
    } else {
      ret["textinput"][0].push_back("There are " + std::to_string(op1) + " groups of " + std::to_string(op2) + " symbols.");
      ret["correct_ans"][0].push_back(nullptr);
    }
    if (ans_blank) {
      ret["textinput"][1].push_back("There are ");
      ret["textinput"][1].push_back("_");
      ret["textinput"][1].push_back(" symbols in all.");
      ret["correct_ans"][1].push_back(nullptr);
      ret["correct_ans"][1].push_back(std::to_string(ans));
      ret["correct_ans"][1].push_back(nullptr);
    } else {
      ret["textinput"][1].push_back("There are " + std::to_string(ans) + " symbols in all.");
      ret["correct_ans"][1].push_back(nullptr);
    }
  } else {
    auto idx = 0;
    if (is == "relate-addition-multiplication") {
      auto s = std::string{};
      for (auto i=op2; i; i--) {
        s += std::to_string(op1);
        if (i > 1)
          s += " + ";
      }
      s += " = ";
      ret["textinput"][idx][0] = s;
      ret["textinput"][idx][1] = "_";
      ret["correct_ans"][idx][0] = nullptr;
      ret["correct_ans"][idx][1] = std::to_string(ans);
      idx ++;
      ret["textinput"][idx] = nlohmann::json::array();
      ret["correct_ans"][idx] = nlohmann::json::array();
    }
    if (op1_blank) {
      ret["textinput"][idx].push_back("_");
      ret["correct_ans"][idx].push_back(std::to_string(op1));
    } else {
      ret["textinput"][idx].push_back(std::to_string(op1));
      ret["correct_ans"][idx].push_back(nullptr);
    }
    ret["textinput"][idx].push_back(" &times; ");
    ret["correct_ans"][idx].push_back(nullptr);
    if (op2_blank) {
      ret["textinput"][idx].push_back("_");
      ret["correct_ans"][idx].push_back(std::to_string(op2));
    } else {
      ret["textinput"][idx].push_back(std::to_string(op2));
      ret["correct_ans"][idx].push_back(nullptr);
    }
    ret["textinput"][idx].push_back(" = ");
    ret["correct_ans"][idx].push_back(nullptr);
    if (ans_blank) {
      ret["textinput"][idx].push_back("_");
      ret["correct_ans"][idx].push_back(std::to_string(ans));
    } else {
      ret["textinput"][idx].push_back(std::to_string(ans));
      ret["correct_ans"][idx].push_back(nullptr);
    }
  }
}

void
IdentifyExprMseg::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  std::string fcol, bkcol, symbol;
  const std::string is {config["is"].get<std::string>()};
  auto mefa = "multiplication-expr-for-arrays" == is;
  auto write_mefa = "write-multiplication-expr-for-arrays" == is;

  /* choose op */
  std::string op;
  do {
    auto i = rand->get() % config["op_types"].size();
    op = config["op_types"][i].get<std::string>();
  } while (false);

  /* init */
  auto min_digits = config["min_digits"].get<uint32_t>();
  auto max_digits = config["max_digits"].get<uint32_t>();
  do {
    auto i = rand->get() % Genmain::htmlColors.size();
    fcol = Genmain::htmlColors[i];
  } while (false);
  do {
    auto i = rand->get() % Genmain::htmlBackgroundColors.size();
    bkcol = Genmain::htmlBackgroundColors[i];
  } while (false);
  do {
    auto i = rand->get() % Genmain::symbols.size();
    symbol = Genmain::symbols[i];
  } while (false);

  /* */
  ret["text"] = "Fill blanks to describe the model:";
  ret["draw_symbols"] = nlohmann::json::array();

  /* generate the test */
  auto op1 = rand->get_digits(min_digits, max_digits) + 1; /* no 0's */
  auto op2 = rand->get_digits(min_digits, max_digits) + 1;
  auto ans = op1 * op2;
  {
    auto o = nlohmann::json::object();
    o["rows"] = nlohmann::json::array();
    o["height"] = 800;
    o["width"] = 800;
    o["box"] = ! (mefa || write_mefa);
    o["cols"] = (mefa || write_mefa) ? 12 : 3;
    o["box_color"] = bkcol;
    for (auto i=op1; i; i--) {
      o["rows"].push_back(nlohmann::json::object
                          ({
                            {"symbol", symbol},
                            {"num", op2},
                            {"color", fcol},
                          })
                          );
    }
    ret["draw_symbols"].push_back(o);
  }

  /* */
  if (write_mefa) {
    auto op1_blank = (rand->get() & 1) ? true : false;
    auto op2_blank = (rand->get() & 1) ? true : false;
    auto ans_blank = ((!op1_blank) && (!op2_blank)) ? true :
      (rand->get() & 1) ? true : false;
    ret["textinput"] = nlohmann::json::array();
    ret["textinput"][0] = nlohmann::json::array();
    ret["correct_ans"] = nlohmann::json::array();
    ret["correct_ans"][0] = nlohmann::json::array();
    ret["textinput"][0][0] = op1_blank ? "_" : std::to_string(op1);
    op1_blank ?
      ret["correct_ans"][0][0] = std::to_string(op1) : ret["correct_ans"][0][0] = nullptr;
    ret["textinput"][0][1] = " &times; ";
    ret["correct_ans"][0][1] = nullptr;
    ret["textinput"][0][2] = op2_blank ? "_" : std::to_string(op2);
    op2_blank ?
      ret["correct_ans"][0][2] = std::to_string(op2) : ret["correct_ans"][0][2] = nullptr;
    ret["textinput"][0][3] = " = ";
    ret["correct_ans"][0][3] = nullptr;
    ret["textinput"][0][4] = ans_blank ? "_" : std::to_string(ans);
    ans_blank ?
      ret["correct_ans"][0][4] = std::to_string(ans) : ret["correct_ans"][0][4] = nullptr;
  } else { /* mefa & (!mefa && !write_mefa)*/
    ret["btnchoice"] = nlohmann::json::array();
    ret["correct_ans"] = nlohmann::json::array();
    ret["btnchoice"].push_back(std::to_string(op1-1) + " &times; " + std::to_string(op2));
    ret["btnchoice"].push_back(std::to_string(op1) + " &times; " + std::to_string(op2-1));
    ret["btnchoice"].push_back(std::to_string(op1-1) + " &times; " + std::to_string(op2+1));
    ret["btnchoice"].push_back(std::to_string(op1+1) + " &times; " + std::to_string(op2-1));
    ret["btnchoice"].push_back(std::to_string(op1) + " &times; " + std::to_string(op2+1));
    ret["btnchoice"].push_back(std::to_string(op1+1) + " &times; " + std::to_string(op2));
    std::shuffle(std::begin(ret["btnchoice"]), std::end(ret["btnchoice"]), rand->rng);

    /* select ans loc */
    auto ans_idx = rand->get() % ret["btnchoice"].size();
    ret["btnchoice"][ans_idx] = std::to_string(op1) + " &times; " + std::to_string(op2);
    ret["correct_ans"].push_back(ans_idx);
  }
}

void
Tables::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  const std::string is{config["is"].get<std::string>()};

  /* choose op */
  std::string op;
  do {
    auto i = rand->get() % config["op_types"].size();
    op = config["op_types"][i].get<std::string>();
  } while (false);

  /* config */
  uint32_t tfor;
  do {
    auto i = rand->get() % config["table_for"].size();
    tfor = config["table_for"][i].get<uint32_t>();
  } while (false);
  uint32_t tmax = config["table_maxin"].get<uint32_t>();

  /* */
  auto op1 = rand->get_range(1, tmax);
  auto ans = (op == "*") ? op1 * tfor :
    (op == "/") ? op1 * tfor :
    (op == "+") ? op1 + tfor : op1 - tfor;
  if (op == "/") {
    auto t = op1;
    op1 = ans;
    ans = t;
  }

  /* */
  auto truefalse_yesno = (rand->get() & 1) ? true : false;
  auto truefalse_yesno_truefalse = (rand->get() & 1) ? true : false;
  if (is == "tables") {
    ret["text"] = "Compute: ";
    ret["textinput"] = nlohmann::json::array();
    ret["textinput"][0] = nlohmann::json::array();
    /* business logic */
    ret["textinput"][0][0] = (("*" == op) || ("/" == op))? std::to_string(op1) : "_";
    ret["textinput"][0][1] = std::string((op == "*") ? "&times;" : ((op == "/") ? "&divide;" : op)) +
      " " + std::to_string(tfor) + " = ";
    ret["textinput"][0][2] = (("*" == op) || ("/" == op)) ? "_" : std::to_string(ans);
    /* business logic: end */
    ret["correct_ans"] = nlohmann::json::array();
    ret["correct_ans"][0] = nlohmann::json::array();
    if (("*" == op) || ("/" == op)) {
      ret["correct_ans"][0][0] = nullptr;
      ret["correct_ans"][0][1] = nullptr;
      ret["correct_ans"][0][2] = std::to_string(ans);
    } else {
      ret["correct_ans"][0][0] = std::to_string(op1);
      ret["correct_ans"][0][1] = nullptr;
      ret["correct_ans"][0][2] = nullptr;
    }
  } else if ((is == "tables-truefalse") && truefalse_yesno) {
    ret["btnchoice"] = nlohmann::json::array();
    ret["correct_ans"] = nlohmann::json::array();
    if (truefalse_yesno_truefalse) {
      ret["text"] = "Is number sentence true or false : **" + std::to_string(op1) +
        " &times; " + std::to_string(tfor) + " = " + std::to_string(ans) + "**";
      auto o = (rand->get() & 1) ? true : false;
      if (o) {
        ret["btnchoice"].push_back("true");
        ret["btnchoice"].push_back("false");
        ret["correct_ans"].push_back(0);
      } else {
        ret["btnchoice"].push_back("false");
        ret["btnchoice"].push_back("true");
        ret["correct_ans"].push_back(1);
      }
    } else {
      ret["text"] = "Is number sentence true or false : **" + std::to_string(op1) +
        " &times; " + std::to_string(tfor) + " = " + std::to_string(ans-2) + "**";
      auto o = (rand->get() & 1) ? true : false;
      if (o) {
        ret["btnchoice"].push_back("true");
        ret["btnchoice"].push_back("false");
        ret["correct_ans"].push_back(1);
      } else {
        ret["btnchoice"].push_back("false");
        ret["btnchoice"].push_back("true");
        ret["correct_ans"].push_back(0);
      }
    }
  } else if (is == "tables-truefalse") {
    ret["text"] = std::string("Select *all sentences* those are **") + (truefalse_yesno_truefalse?"true":"false") + "**";
    ret["btnchoice"] = nlohmann::json::array();
    ret["correct_ans"] = nlohmann::json::array();
    for (uint32_t i=0; i<6; i++) {
      op1 = rand->get_range(0, tmax);
      ans = op1 * tfor;
      auto o = rand->get() & 7;
      if (o == 0)
        ret["btnchoice"].push_back(std::to_string(op1-1) + " &times; " + std::to_string(tfor) + " = " + std::to_string(ans));
      else if (o == 1)
        ret["btnchoice"].push_back(std::to_string(op1) + " &times; " + std::to_string(tfor-1) + " = " + std::to_string(ans));
      else if (o == 2)
        ret["btnchoice"].push_back(std::to_string(op1) + " &times; " + std::to_string(tfor+1) + " = " + std::to_string(ans));
      else if (o == 3)
        ret["btnchoice"].push_back(std::to_string(op1+1) + " &times; " + std::to_string(tfor) + " = " + std::to_string(ans));
      else if (o == 4)
        ret["btnchoice"].push_back(std::to_string(op1+1) + " &times; " + std::to_string(tfor-1) + " = " + std::to_string(ans));
      else if (o == 5)
        ret["btnchoice"].push_back(std::to_string(op1-1) + " &times; " + std::to_string(tfor+1) + " = " + std::to_string(ans));
      else { /* true case probability: 3/8 */
        ret["btnchoice"].push_back(std::to_string(op1) + " &times; " + std::to_string(tfor) + " = " + std::to_string(ans));
      }
      if (truefalse_yesno_truefalse && (o > 5))
        ret["correct_ans"].push_back(i);
      else if ((!truefalse_yesno_truefalse) && (o <= 5))
        ret["correct_ans"].push_back(i);
    }
  } else if (is == "tables-sorting") {
    /* FIXME */
  }
}


void
AdditionPatternIncreasingPlacevalues::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  /* choose op */
  std::string op;
  do {
    auto i = rand->get() % config["op_types"].size();
    op = config["op_types"][i].get<std::string>();
  } while (false);

  /* static conf */
  ret["text"] = std::string("Complete the **") + ((op == "+") ? "Addition" : "Subtraction") + "** pattern table over increasing place values:";
  ret["no-outlines"] = true;

  auto min_digits = config["min_digits"].get<uint32_t>();
  auto max_digits = config["max_digits"].get<uint32_t>();
  std::vector<uint32_t> vars;
  auto num_vars = config.contains("num_vars") ? config["num_vars"].get<uint32_t>() : 2;
  auto num_steps = config.contains("num_steps") ? config["num_steps"].get<uint32_t>() : 3;

  /* get all vars */
  uint32_t res = 0;
  for (auto i=0; i<num_vars-1; i++) {
    vars.emplace_back(rand->get_digits(min_digits, max_digits));
    res += vars.back();
  }
  do {
    auto d = rand->get_digits(min_digits, max_digits);
    if (op == "+") {
      res += d;
      vars.emplace_back(d);
    } else /* op == "-" */ {
      vars.insert(vars.begin(), d + res);
      res = d;
    }
  } while (false);

  /* init */
  auto is_q_res = (rand->get() & 1) ? true : false;
  is_q_res = false;
  ret["th"] = nlohmann::json::array({nullptr, nullptr});
  ret["htabledata"] = nlohmann::json::array();
  ret["correct_ans"] = nlohmann::json::array();

  /* generate  */
  std::stringstream sout;
  uint32_t mult = 1;
  if (is_q_res) {
    for (auto step=0; step<num_steps; step++, mult *= 10) {
      sout.str(std::string());
      for (auto var=0; var<num_vars; var++) {
        sout << std::dec << (vars[var]*mult) << " " << ((var < (num_vars-1)) ? op : "=") << " ";
      }
      ret["htabledata"].push_back(nlohmann::json::array());
      ret["htabledata"].back().push_back(sout.str());
      ret["htabledata"].back().push_back("_");
      ret["correct_ans"].push_back(nlohmann::json::array());
      ret["correct_ans"].back().push_back(nullptr);
      ret["correct_ans"].back().push_back(std::to_string(res*mult));
    }
  } else {
    ret["th"].push_back(nullptr);
    for (auto step=0; step<num_steps; step++, mult *= 10) {
      sout.str(std::string());
      for (auto var=0; var<num_vars-1; var++) {
        sout << std::dec << (vars[var]*mult) << " " << op << " ";
      }
      ret["htabledata"].push_back(nlohmann::json::array());
      ret["htabledata"].back().push_back(sout.str());
      ret["htabledata"].back().push_back("_");
      sout.str(std::string()); sout << "= " << (res*mult);
      ret["htabledata"].back().push_back(sout.str());
      ret["correct_ans"].push_back(nlohmann::json::array());
      ret["correct_ans"].back().push_back(nullptr);
      ret["correct_ans"].back().push_back(std::to_string(vars.back()*mult));
      ret["correct_ans"].back().push_back(nullptr);
    }
  }

  ret["maxlength"] = max_digits*2;
}

/*
 * this.question = {
 text: 'this is trial dragngroup, pls answer...',
 dragngroup: [
 { text: 'pull from',
 values: [
 "John", "Joao", "Jean", "Gerard", "Juan", "Edgard", "Johnson",
 ]
 },
 { text: 'group 0', values: ['a'] },
 { text: 'group 1', values: ['b'] },
 { text: 'group 2', values: ['c'] },
 ],
 correct_ans: [
 { values: [
 "John", "Joao", "Jean", "Gerard", "Juan", "Edgard", "Johnson",
 ]
 },
 { values: ['a'] },
 { values: ['b'] },
 { values: ['c'] },
 ],
 };

*/
void
DragGroup::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{ /* FIXME: Incomplete */
  /* choose op */
  std::string op;
  do {
    auto i = rand->get() % config["op_types"].size();
    op = config["op_types"][i].get<std::string>();
  } while (false);

  // ret["text"] = "my question";
  // ret["dragngroup"] =
}

void
Expression::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  /* init */
  auto num_vars = config["num_vars"].get<uint32_t>();
  optreeState state;
  for (auto&tt : config["test_type"]) {
    auto n = tt.get<std::string>();
    if ("missing-val" == n) state.is_missingval = true;
    else if ("missing-op" == n) state.is_missingop = true;
    else if ("missing-in" == n) state.is_missingin = true;
    else if ("findx" == n) state.is_missingin = state.is_findx = true;
    else if ("findx2" == n) state.is_findx2 = state.is_findx = true;
    else if ("findx2x" == n) state.is_findx2x = state.is_findx = true;
    else if ("negints" == n) state.is_negints = true;
  }
  auto sel_node = rand->get_range(0, num_vars-1); /* number of operators are 1 less than vars */
  uint32_t id=0;
  auto op1 = OpTree{id, num_vars, sel_node};

  for (uint32_t ui=0; ui<10; ui++) {
    op1.randomize(rand, config, state);
    if (! state.is_negints)
      if (op1.v > 0) break;
  }

  ret["text"] = state.is_missingop ? "Find missing operator:" : state.is_findx ? "Find X:" : state.is_missingin ? "Find missing value:" : "Evaluate:";
  ret["textinput"] = nlohmann::json::array();
  ret["textinput"][0] = nlohmann::json::array();
  ret["textinput"][0].push_back("$" + op1.to_tex(false, state.is_missingin, state.is_missingop, state.is_findx) + "\\ =" + (state.is_missingval ? "" : (std::to_string(op1.v))) + "$");
  ret["correct_ans"] = nlohmann::json::array();
  ret["correct_ans"][0] = nlohmann::json::array();
  if (state.is_findx || state.is_missingin) {
    ret["correct_ans"][0].push_back(nullptr);
    ret["textinput"][1] = nlohmann::json::array();
    if (state.is_findx) {
      ret["textinput"][1].push_back("$x =$");
    } else {
      ret["textinput"][1].push_back("$? =$");
    }
    ret["textinput"][1].push_back("_");
    ret["correct_ans"][1] = nlohmann::json::array();
    ret["correct_ans"][1].push_back(nullptr);
    ret["correct_ans"][1].push_back(state.is_missingval ? std::to_string(op1.v) : op1.get_sel_ans(state.is_missingin, state.is_missingop, state.is_findx));
    if (state.is_findx2 || state.is_findx2x) {
      sel_node = rand->get_range(0, num_vars-1);
      id = 0;
      auto op2 = OpTree{id, num_vars, sel_node};
      if (state.is_findx2x) config["xval"] = std::stoi(ret["correct_ans"][1][1].get<std::string>());
      for (uint32_t ui=0; ui<10; ui++) {
        op2.randomize(rand, config, state);
        if (! state.is_negints)
          if (op2.v > 0) break;
      }
      ret["textinput"][0][0] = "$" + op1.to_tex(false, state.is_missingin, state.is_missingop, state.is_findx) + "\\ =\\ " + "(" + op2.to_tex(false, false, false, state.is_findx2x) + ")" + (op1.v > op2.v ? ("+" + std::to_string(op1.v-op2.v)) : ( op2.v > op1.v ? ("-"+std::to_string(op2.v-op1.v)) : "" )) + "$";
    }
  } else {
    ret["textinput"][0].push_back("_");
    ret["correct_ans"][0].push_back(nullptr);
    ret["correct_ans"][0].push_back(state.is_missingval ? std::to_string(op1.v) : op1.get_sel_ans(state.is_missingin, state.is_missingop, state.is_findx));
  }
}

void
RatioProblem::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  std::vector<std::string> vsa;
  nlohmann::json ret1;
  (*all_mods)["Maths::Addition"]->controllers["Expression"]->gen(ret1, rand, config);
  boost::algorithm::split(vsa, ret1["textinput"][0][0].get<std::string>(), boost::is_any_of("{}"));
  auto v = std::stoi(ret1["correct_ans"][0][1].get<std::string>());
  auto min_num = config["min_num"].get<uint32_t>();
  auto max_num = config["max_num"].get<uint32_t>();
  uint32_t m = rand->get_range(min_num, max_num);
  bool x_is_top = rand->get_range(0, 1);
  ret["text"] = "Solve for $x$:";
  ret["textinput"] = nlohmann::json::array();
  ret["textinput"][0] = nlohmann::json::array();
  ret["correct_ans"] = nlohmann::json::array();
  ret["correct_ans"][0] = nlohmann::json::array();
  ret["textinput"][0][0] = "$\\frac{" + vsa[3] + "}{" + vsa[1] + "} = \\frac{" +
    (x_is_top ? ("x}{" + std::to_string(m*v)) : (std::to_string(m)+"}{x")) +
    "}$";
  ret["correct_ans"][0][0] = x_is_top ? std::to_string(m) : std::to_string(m*v);
}

/* create global object */
namespace {
  Maths::Addition a{};
}
