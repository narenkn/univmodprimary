#include <random>
#include <sstream>
#include "nlohmann/json.hpp"
#ifdef SABUILD
#include "sa/genmain.h"
#else
# ifdef WASMBUILD
#include "src/genmain.h"
# else
#include "top/genmain.h"
# endif
#endif
#include "numbertheory.h"

namespace {

  std::string
  to_roman(uint32_t value)
  {
    struct romandata_t { uint32_t value; char const* numeral; };
    static romandata_t const romandata[] =
      { 1000, "M",
        900, "CM",
        500, "D",
        400, "CD",
        100, "C",
        90, "XC",
        50, "L",
        40, "XL",
        10, "X",
        9, "IX",
        5, "V",
        4, "IV",
        1, "I",
        0, NULL }; // end marker

    std::string result;
    for (romandata_t const* current = romandata; current->value > 0; ++current) {
      while (value >= current->value) {
        result += current->numeral;
        value  -= current->value;
      }
    }
    return result;
  }

  class DivisibilityRules : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

  class NumberLine : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

  class ToRoman : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

  class SkipCount : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

}

Maths::NumberTheory::NumberTheory():
  GenMod(__CLASS_NAME__)
{
  MAKE_CONTROLLER(DivisibilityRules);
  MAKE_CONTROLLER(NumberLine);
  MAKE_CONTROLLER(ToRoman);
  MAKE_CONTROLLER(SkipCount);
}

void
DivisibilityRules::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  /* init */
  auto min_digits = config["min_digits"].get<uint32_t>();
  auto max_digits = config["max_digits"].get<uint32_t>();
  auto div_min_digits = config["div_min_digits"].get<uint32_t>();
  auto div_max_digits = config["div_max_digits"].get<uint32_t>();
  auto num_vars = config["num_vars"].get<uint32_t>();

  /* table-for should be provided */
  uint32_t table_for_idx = rand->get() % config["table_for"].size();
  uint32_t table_for = config["table_for"][table_for_idx].get<uint32_t>();

  /* */
  enum TestType {YesNo, Choose, TT_MAX} test_type;
  test_type = static_cast<TestType>(rand->get() % TT_MAX);
  if (config["test_type"].get<std::string>() == "YesNo") test_type = YesNo;

  /* structures */
  ret["btnchoice"] = nlohmann::json::array();
  ret["correct_ans"] = nlohmann::json::array();

  /* genrate operands */
  auto op1 = (config["test_type"].get<std::string>() == "random") ?
    rand->get_digits_no0(min_digits, max_digits) : table_for;
  if (YesNo == test_type) {
    auto op2 = rand->get_digits_no0(div_min_digits, div_max_digits);
    ret["text"] = "Is **" + std::to_string(op1) + "** divisible by **" + std::to_string(op2) + "**?";
    auto yesloc = rand->get() & 1;
    auto noloc = yesloc ^ 1;
    /* 0 : Yes, No ; 1 : No, Yes */
    ret["btnchoice"].push_back(yesloc ? "No" : "Yes");
    ret["btnchoice"].push_back(yesloc ? "Yes" : "No");
    /* */
    ret["correct_ans"].push_back((0 == (op1%op2)) ? yesloc : noloc);
  } else {
    /* set op1 for these factors */
    std::vector<uint32_t> factors;
    for (auto ui1=0; ui1<num_vars; ui1++) {
      auto f = rand->get_digits_no0(0, 1);
      if (rand->get_digits(1, 1) & 1) {
        op1 /= f; op1 *= f;
      }
      if (rand->get_digits(1, 1) & 1) factors.push_back(f);
      else factors.insert(factors.begin(), f);
    }
    /* */
    for (auto ui1=0; ui1<num_vars; ui1++) {
      ret["btnchoice"].push_back(std::to_string(factors[ui1]));
      if (0 == (op1 % factors[ui1]))
        ret["correct_ans"].push_back(ui1);
    }
    /* */
    ret["text"] = "Select all factors of **" + std::to_string(op1) + "**?";
  }
}

void
NumberLine::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  /* init */
  auto minval = config["minval"].get<int32_t>();
  auto maxval = config["maxval"].get<int32_t>();
  int32_t op1 = rand->get_range(0, maxval-minval);

  /* */
  ret["datastr"] = nlohmann::json::object();
  ret["datastr"]["sequences"] = nlohmann::json::array();
  ret["datastr"]["op1"] = op1+minval;
  ret["datastr"]["width"] = 400;
  ret["datastr"]["height"] = 125;
  ret["datastr"]["canmark"] = true;
  ret["datastr"]["correct_ans"] = nlohmann::json::array();
  ret["text"] = "Mark: $" + std::to_string(op1+minval) + "$";
  ret["datastr"]["sequences"][0] = nlohmann::json::object();
  ret["datastr"]["sequences"][0]["values"] = nlohmann::json::array();
  ret["datastr"]["sequences"][0]["arcs"] = nlohmann::json::array();
  ret["datastr"]["sequences"][0]["color"] = "black";
  for (int32_t i=minval; i<=maxval; i++) {
    ret["datastr"]["correct_ans"].push_back((op1+minval) == i);
    ret["datastr"]["sequences"][0]["values"].push_back( std::to_string(i) );
  }
}

void
ToRoman::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  /* init */
  auto minval = config["minval"].get<uint32_t>();
  auto maxval = config["maxval"].get<uint32_t>();
  uint32_t op1 = rand->get_range(minval, maxval);

  ret["text"] = "$"+std::to_string(op1)+"$";
  ret["textinput"] = nlohmann::json::array();
  ret["textinput"][0] = nlohmann::json::array();
  ret["textinput"][0][0] = "_";
  ret["correct_ans"] = nlohmann::json::array();
  ret["correct_ans"][0] = nlohmann::json::array();
  ret["correct_ans"][0][0] = to_roman(op1);
}

void
SkipCount::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  /* init */
  auto sb0 = config["skipby"][0].get<uint32_t>();
  auto sb1 = config["skipby"][1].get<uint32_t>();
  auto digits = config["start_digits"].get<uint32_t>();
  auto num_vars = config["num_vars"].get<uint32_t>();
  uint32_t sb = rand->get_range(sb0, sb1);
  uint32_t start = rand->get_digits(digits, digits);
  uint32_t sel = rand->get_range(0, num_vars-1);

  ret["text"] = "";
  ret["textinput"] = nlohmann::json::array();
  ret["textinput"][0] = nlohmann::json::array();
  ret["correct_ans"] = nlohmann::json::array();
  ret["correct_ans"][0] = nlohmann::json::array();
  for (uint32_t ui=0; ui<num_vars; ui++, start+=sb) {
    if (ui) {
      ret["textinput"][0].push_back(",");
      ret["correct_ans"][0].push_back(nullptr);
    }
    ret["textinput"][0].push_back((ui == sel) ? "_" : std::to_string(start));
    if (ui == sel)
      ret["correct_ans"][0].push_back(std::to_string(start));
    else
      ret["correct_ans"][0].push_back(nullptr);
  }
}

/* create global object */
namespace {
  Maths::NumberTheory a{};
}
