#include <random>
#include <sstream>
#include "nlohmann/json.hpp"
#ifdef SABUILD
#include "sa/genmain.h"
#else
# ifdef WASMBUILD
#include "src/genmain.h"
# else
#include "top/genmain.h"
# endif
#endif
#include "fraction.h"
#include <algorithm>
#include <sstream>

namespace {

  struct Fract {
    int32_t n;
    uint32_t d;
    Fract() = default;
    Fract(int32_t a, uint32_t b): n(a), d(b) {}
  };

  Fract
  gen_proper_fract( std::shared_ptr<RandGenerator> rand,
                    uint32_t digits, uint32_t denom=0, bool canbeneg=false, bool isneg=false ) {
    int32_t neum;
    while (denom < 3) {
      denom = rand->get_digits_no0(digits, digits);
    }
    do {
      neum = rand->get_digits_no0(digits, digits);
    } while ((neum >= denom) or (neum < 2));
    if (isneg or (canbeneg and (rand->get() & 1))) {
      neum = -neum;
    }
    return {neum, denom};
  }

  std::string
  get_random_op( std::shared_ptr<RandGenerator> rand,
                 nlohmann::json& config ) {
    auto i = rand->get() % config["op_types"].size();
    return config["op_types"][i].get<std::string>();
  }

  void
  toLowestTerms( Fract &f ) {
    for (uint32_t sqneum = f.n < f.d ? f.n : f.d; sqneum > 1; sqneum--) {
      while ( (0 == (f.n%sqneum)) and (0 == (f.d%sqneum)) ) {
        f.n /= sqneum;
        f.d /= sqneum;
      }
    }
  }

  class Equivalent : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

  class LowestTerms : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

  class Operate : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

  class Illustrate : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

  class MarkOnNumberLines : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

  class GenericTestRandom : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { ret["result"] = true; }
  };

}

Maths::Fraction::Fraction():
  GenMod(__CLASS_NAME__)
{
  MAKE_CONTROLLER(Equivalent);
  MAKE_CONTROLLER(LowestTerms);
  MAKE_CONTROLLER(Operate);
  MAKE_CONTROLLER(Illustrate);
  MAKE_CONTROLLER(MarkOnNumberLines);
  MAKE_CONTROLLER(GenericTestRandom);
}

void
Equivalent::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  /* init */
  auto digits = config["digits"].get<uint32_t>();
  auto multiplier_digits = config["multiplier_digits"].get<uint32_t>();
  auto num_vars = config["num_vars"].get<uint32_t>();

  /* */
  auto mult = rand->get_digits_no0(digits, digits);
  auto f1 = gen_proper_fract(rand, digits);
  toLowestTerms(f1);

  /* */
  ret["btnchoice"] = nlohmann::json::array();
  ret["correct_ans"] = nlohmann::json::array();
  std::vector<Fract> opts;
  for(uint32_t ui=0; (ui<num_vars) or (0 == ret["correct_ans"].size()); ui++) {
    std::stringstream text;
    auto o = gen_proper_fract(rand, multiplier_digits);
    if ((rand->get() & 7) < 4) {
      o.d = o.n * f1.d / f1.n;
    }
    opts.push_back(o);
    toLowestTerms(o);
    text << "$\\frac{" << o.n << "}" << "{" << o.d << "}$";
    if ((o.n == f1.n) and (o.d == f1.d))
      ret["correct_ans"].push_back(ui);
    ret["btnchoice"].push_back(text.str());
  }

  {
    std::stringstream text;
    text << "Which fraction " << ((1 == ret["correct_ans"].size()) ? "is" : "are") << " equivalent to ";
    text << "$\\frac{" << (f1.n*mult) << "}" << "{" << (f1.d*mult) << "}$";
    ret["text"] = text.str();
  }
}

void
LowestTerms::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  /* init */
  auto digits = config["digits"].get<uint32_t>();
  auto multiplier_digits = config["multiplier_digits"].get<uint32_t>();
  auto num_vars = config["num_vars"].get<uint32_t>();
  auto test_type = config["test_type"].get<std::string>();

  /* */
  auto f = gen_proper_fract(rand, digits);

  /* reduce to its bare parts */
  toLowestTerms(f);

  /* create the fraction */
  uint32_t op1, ans;
  std::string s_ans;
  if ( ("convert2dec" == test_type) or ("convert2frac" == test_type) or
       ("convertdec2frac" == test_type) or ("convert2pcent" == test_type) ) {
    s_ans = std::to_string( f.n );
    f.n *= f.d;
    for (uint32_t ui=0; ui<2; ui++) f.d *= 10;
    if ("convert2pcent" != test_type) {
      if (s_ans.length() < 2) {
        s_ans = "0.0" + s_ans;
      } else if (s_ans.length() == 2) {
        s_ans = "0." + s_ans;
      } else {
        s_ans.insert(2, ".");
      }
    }
    if ( ("convert2dec" == test_type) or ("convert2pcent" == test_type) ) {
      ret["text"] = "Convert $\\frac{" + std::to_string(f.n) + "}{" + std::to_string(f.d) + "}$ to " + (("convert2pcent" == test_type) ? "percentage?" : "decimal?");
    } else {
      toLowestTerms(f);
      ret["text"] = "Convert $" + s_ans + "$ to fraction $\\frac{N}{D}$";
    }
  } else if ("fractionof" == test_type) {
    op1 = rand->get_digits_no0(multiplier_digits, multiplier_digits);
    ans = op1 * f.n;
    op1 *= f.d;
    ret["text"] = "what is : $\\frac{" + std::to_string(f.n) + "}{" + std::to_string(f.d) + "}$ of $" + std::to_string(op1) + "$ ?";
  } else if ("1minus" == test_type) {
    ret["text"] = "Compute : $1 - \\frac{" + std::to_string(f.d-f.n) + "}{" + std::to_string(f.d) + "} = \\frac{N}{D}$";
  } else {
    uint32_t mneum = f.n, mdenom = f.d;
    for (uint32_t ui1=num_vars; ui1; ui1--) {
      auto m = rand->get_digits_no0(multiplier_digits, multiplier_digits);
      mneum *= m, mdenom *= m;
    }
    ret["text"] = "Write the fraction in lowest terms : $\\frac{" + std::to_string(mneum) + "}{" + std::to_string(mdenom) + "} = \\frac{N}{D}$";
  }
  /* */
  ret["textinput"] = nlohmann::json::array();
  ret["textinput"][0] = nlohmann::json::array();
  ret["correct_ans"] = nlohmann::json::array();
  ret["correct_ans"][0] = nlohmann::json::array();
  ret["th"] = nlohmann::json::array({nullptr});
  if ( ("convert2dec" == test_type) or ("fractionof" == test_type) or
       ("convert2pcent" == test_type) ) {
    ret["textinput"][0].push_back("_");
    ret["correct_ans"][0].push_back((("convert2dec" == test_type) or ("convert2pcent" == test_type)) ? s_ans : std::to_string(ans));
  } else {
    ret["textinput"][0].push_back("N = ");
    ret["textinput"][0].push_back("_");
    ret["textinput"][0].push_back(", D = ");
    ret["textinput"][0].push_back("_");
    ret["correct_ans"][0].push_back(nullptr), ret["correct_ans"][0].push_back(std::to_string(f.n)), ret["correct_ans"][0].push_back(nullptr), ret["correct_ans"][0].push_back(std::to_string(f.d));
  }
}

void
Operate::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  /* init */
  auto digits = config["digits"].get<uint32_t>();
  auto multiplier_digits = config["multiplier_digits"].get<uint32_t>();
  auto num_vars = config["num_vars"].get<uint32_t>();
  auto like_fraction = config["like_fraction"].get<bool>();
  auto multiply_whole = config["multiply_whole"].get<bool>();
  auto f = gen_proper_fract(rand, digits);

  /* ops[0] contains the operator for multiply_whole, rest is for other operands
   */
  std::vector<Fract> vars;
  std::vector<std::string> ops;
 redo_gen:
  vars.clear(); ops.clear();
  if (true) {
    std::vector<uint32_t> nvars;
    for (auto ui=0; ui<num_vars+1; ui++) {
      nvars.push_back(rand->get_digits_no0(digits, digits));
    }
    std::sort(nvars.begin(), nvars.end(), std::greater<>());

    for (auto ui=0; ui<num_vars; ui++) {
      vars.emplace_back(nvars[ui+1], like_fraction ? nvars[0] : nvars[rand->get() & 1]);
      ops.emplace_back(get_random_op(rand, config));
      // std::cout << "vars:" << vars.back().n << "/" << vars.back().d << " ops:" << ops.back() << std::endl;
    }
  }

  /* */
  uint32_t m;
  Fract exp{1, 1};
  /* compute result */
  for (auto ui=0; ui<num_vars; ui++) {
    if (("+" == ops[ui]) or ("-" == ops[ui])) {
      if (0 == ui) {
        exp.n = vars[ui].n;
        exp.d = vars[ui].d;
      } else {
        // std::cout << "exp.n:" << exp.n << " vars.n:" << vars[ui].n << std::endl;
        // std::cout << "comp:" << int32_t(vars[ui].d * exp.n) << " " << int32_t((("+" == ops[ui]) ? 1 : -1) * int32_t(exp.d * vars[ui].n)) << std::endl;
        // std::cout << "comp2:" << ( int32_t(vars[ui].d * exp.n) + int32_t((("+" == ops[ui]) ? 1 : -1) * int32_t(exp.d * vars[ui].n)) ) << std::endl;
        exp.n = int32_t(vars[ui].d * exp.n) + int32_t((("+" == ops[ui]) ? 1 : -1) * int32_t(exp.d * vars[ui].n));
        exp.d *= vars[ui].d;
      }
    } else {
      if ((0 == ui) or ("*" == ops[ui])) {
        exp.n *= vars[ui].n;
        exp.d *= vars[ui].d;
      } else {
        exp.n *= vars[ui].d;
        exp.d *= vars[ui].n;
      }
      if (multiply_whole) {
        m = rand->get_digits_no0(multiplier_digits, multiplier_digits);
        if ("*" == ops[0])
          exp.n *= m;
        else
          exp.d *= m;
      }
    }
  }
  toLowestTerms(exp);
  if ((exp.n >= exp.d) or (exp.n <= 0)) goto redo_gen;

  /* */
  {
    std::stringstream text;
    text << "Express in lowest terms: $";
    for (auto ui=0; ui<num_vars; ui++) {
      if (ui)
        text << ("*" == ops[ui] ? " \\times " :
                 ("/" == ops[ui] ? " \\div " : ops[ui]));
      text << "\\frac{" << vars[ui].n << "}{" << vars[ui].d << "}";
    }
    if (multiply_whole) {
      text << ("*" == ops[0] ? " \\times " : " \\div ");
      text << m;
    }
    text << " = \\frac{N}{D}$";
    ret["text"] = text.str();
  }
  ret["textinput"] = nlohmann::json::array();
  ret["textinput"][0] = nlohmann::json::array();
  ret["textinput"][0].push_back("N = ");
  ret["textinput"][0].push_back("_");
  ret["textinput"][0].push_back(", D = ");
  ret["textinput"][0].push_back("_");
  ret["correct_ans"] = nlohmann::json::array();
  ret["correct_ans"][0] = nlohmann::json::array();
  ret["correct_ans"][0].push_back(nullptr);
  ret["correct_ans"][0].push_back(std::to_string(exp.n));
  ret["correct_ans"][0].push_back(nullptr);
  ret["correct_ans"][0].push_back(std::to_string(exp.d));
}

void
Illustrate::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  /* init */
  auto digits = config["digits"].get<uint32_t>();
  auto maxlen = config["maxlen"].get<uint32_t>();

  uint32_t op1{(rand->get() % maxlen)+1}, op2{(rand->get() % maxlen)+1};
  Fract f1{(int32_t)op1, op2};
  toLowestTerms(f1);

  /* */
  ret["datastr"] = nlohmann::json::object();
  ret["datastr"]["os"] = nlohmann::json::array();
  ret["datastr"]["xticks"] = maxlen;
  ret["datastr"]["yticks"] = 3;
  ret["datastr"]["size"] = 0;
  ret["datastr"]["width"] = 400;
  ret["datastr"]["height"] = 125;
  ret["datastr"]["imgsz"] = 50;
  ret["datastr"]["candot"] = false;
  ret["datastr"]["dots"] = nlohmann::json::array();
  for (uint32_t ui=0; ui<2; ui++) {
    ret["datastr"]["os"].push_back(nlohmann::json::object());
    ret["datastr"]["os"][ui]["type"] = "line";
    ret["datastr"]["os"][ui]["color"] = "Black";
    ret["datastr"]["os"][ui]["x1"] = 0;
    ret["datastr"]["os"][ui]["y1"] = ui+1;
    ret["datastr"]["os"][ui]["x2"] = (0 == ui) ? op1 : op2;
    ret["datastr"]["os"][ui]["y2"] = ui+1;
  }
  ret["text"] = "";
  ret["textinput"] = nlohmann::json::array();
  ret["textinput"][0] = nlohmann::json::array();
  ret["textinput"][0][0] = "N = ";
  ret["textinput"][0][1] = "_";
  ret["textinput"][0][2] = ", D = ";
  ret["textinput"][0][3] = "_";
  ret["correct_ans"] = nlohmann::json::array();
  ret["correct_ans"][0] = nlohmann::json::array();
  ret["correct_ans"][0][0] = nullptr;
  ret["correct_ans"][0][1] = std::to_string(f1.n);
  ret["correct_ans"][0][2] = nullptr;
  ret["correct_ans"][0][3] = std::to_string(f1.d);
}

void
MarkOnNumberLines::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  /* init */
  auto minval = config["minval"].get<uint32_t>();
  auto maxval = config["maxval"].get<uint32_t>();
  auto is_decimal = config["is_decimal"].get<bool>();

  /* generate */
  uint32_t op1, t_op, op2;
  if (is_decimal) {
    maxval = (rand->get()&1) ? 10 : 20;
    op2 = maxval;
    op1 = rand->get_range(1, maxval-1);
  } else {
    op1 = rand->get_range(minval, maxval-2);
    t_op = rand->get_range(minval, maxval-2);
    op2 = (op1 > t_op) ? op1 : t_op; op1 = (op1 > t_op) ? t_op : op1;
    if (op1 == op2) op2++;
  }

  /* reduce */
  Fract f1{(int32_t)op1, op2};
  toLowestTerms(f1);

  /* */
  ret["datastr"] = nlohmann::json::object();
  ret["datastr"]["sequences"] = nlohmann::json::array();
  ret["datastr"]["op1"] = op1; ret["datastr"]["op2"] = op2;
  ret["datastr"]["width"] = 400;
  ret["datastr"]["height"] = 125;
  ret["datastr"]["canmark"] = true;
  ret["datastr"]["correct_ans"] = nlohmann::json::array();
  ret["text"] = is_decimal ?
    "Decimal: $" + std::to_string(((float)f1.n)/f1.d) + "$" :
    "Fraction: $" + std::to_string(f1.n) + "/" + std::to_string(f1.d) + "$";
  ret["datastr"]["sequences"][0] = nlohmann::json::object();
  ret["datastr"]["sequences"][0]["values"] = nlohmann::json::array();
  ret["datastr"]["sequences"][0]["arcs"] = nlohmann::json::array();
  ret["datastr"]["sequences"][0]["color"] = "black";
  for (uint32_t ui=0; ui<=maxval; ui++) {
    ret["datastr"]["correct_ans"][ui] = (op1 == ui);
    ret["datastr"]["sequences"][0]["values"][ui] = (0 == ui) ? "0" : (op2 == ui) ? "1" : "";
  }
}


void
GenericTestRandom::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  /* init */
  auto nint = config["nint"].get<uint32_t>();
  auto nfract = config["nfract"].get<uint32_t>();
  auto digits = config["digits"].get<uint32_t>();

  /* */
  ret["ints"] = nlohmann::json::array();
  ret["nfract"] = nlohmann::json::array();
  for (auto ui=0; ui<nint; ui++) {
    ret["ints"].push_back(rand->get());
  }
  for (auto ui=0; ui<nfract; ui++) {
    auto f1 = gen_proper_fract(rand, digits);
    toLowestTerms(f1);
    ret["fracts"].push_back(nlohmann::json::array());
    ret["fracts"].back().push_back(f1.n);
    ret["fracts"].back().push_back(f1.d);
  }
  ret["correct_ans"] = nlohmann::json::array();
  ret["correct_ans"][0] = nlohmann::json::array();
  ret["multiinput"] = nlohmann::json::array();
  ret["multiinput"][0] = nlohmann::json::array();
  ret["text"] = "";
}

/* create global object */
namespace {
  Maths::Fraction a{};
}
