#include <random>
#include <sstream>
#include "nlohmann/json.hpp"
#ifdef SABUILD
#include "sa/genmain.h"
#else
# ifdef WASMBUILD
#include "src/genmain.h"
# else
#include "top/genmain.h"
# endif
#endif
#include "placevalue.h"
#include <boost/algorithm/string/join.hpp>

namespace {

  class FindDigit : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

}

Maths::PlaceValue::PlaceValue():
  GenMod(__CLASS_NAME__)
{
  MAKE_CONTROLLER(FindDigit);
}

namespace {
  void
  s2e_correct_ans(uint32_t op, uint32_t depth, std::vector<std::string>& ca)
  {
    if (0 == op) return;
    auto v = op % 10;
    s2e_correct_ans(op / 10, depth+1, ca);
    for (; depth>1; depth--) { v *= 10; }
    if (v)
      ca.push_back(std::to_string(v));
  }

  const char* plnames[] = {
    "Millionths", "Hundred Thousandths", "Ten Thousandths", /* -6, -5, -4 */
    "Thousandths", "Hundredths", "Tenths", /* -3, -2, -1 */
    "Ones", "Tens", "Hundreds", "Thousands", "Ten Thousands", /* 0, 1, 2, 3 */
    "Hundred Thousands", "Millions", "Ten Millions", /* 4, 5, 6 */
    "Hundred Millions", "Billions", "Ten Billions", /* 7, 8, 9 */
    "Hundred Billions", /* 10 */
  };
  const uint32_t plname_decimalat = 6;

}

void
FindDigit::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  /* init */
  auto test_type = config["test_type"].get<std::string>();
  auto min_digits = config["min_digits"].get<uint32_t>();
  auto max_digits = config["max_digits"].get<uint32_t>();
  auto op1 = rand->get_digits_no0(min_digits+max_digits, min_digits+max_digits);
  uint32_t sel_digit;
  std::string op1s{std::to_string(op1)};

  /* insert . */
  if (min_digits != 0) {
    uint32_t ii = op1s.length()-min_digits;
    op1s.insert(ii, ".");
  }

  /* bold */
  do {
    sel_digit = rand->get_range(0, min_digits+max_digits-1);
  } while(sel_digit >= op1s.length());
  uint32_t ii = op1s.length()-sel_digit-1;
  if (min_digits != 0)
    ii += (sel_digit >= min_digits) ? -1 : 0;

  /* structures */
  ret["textinput"] = nlohmann::json::array();
  ret["textinput"][0] = nlohmann::json::array();
  ret["correct_ans"] = nlohmann::json::array();
  ret["correct_ans"][0] = nlohmann::json::array();
  if ("placevalue" == test_type) {
    op1s.insert(ii+1, "**");
    op1s.insert(ii, "**");
    ret["text"] = "What is the place value of highlighted digit of: " + op1s;
    ret["textinput"][0][0] = "_";
    ret["correct_ans"][0][0] = plnames[plname_decimalat+sel_digit-min_digits];
  } else {
    ret["text"] = "What digit is at **" + std::string(plnames[plname_decimalat+sel_digit-min_digits]) + "** place value in: " + op1s;
    ret["textinput"][0][0] = "_";
    ret["correct_ans"][0][0] = op1s.substr(ii, 1);
  }
}

/* create global object */
namespace {
  Maths::PlaceValue a{};
}
