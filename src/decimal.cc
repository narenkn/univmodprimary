#include <random>
#include <sstream>
#include "nlohmann/json.hpp"
#ifdef SABUILD
#include "sa/genmain.h"
#else
# ifdef WASMBUILD
#include "src/genmain.h"
# else
#include "top/genmain.h"
# endif
#endif
#include "decimal.h"

namespace {

  void
  reversess(std::stringstream& ss, const std::string str) {
    for (auto i = str.length(); i; i--)
      ss << str[i-1];
  }

  const char* ors[] = {"th", "st", "nd", "rd"};
  const char*
  getOrdinal(uint32_t n)
  {
    n %= 100;
    if ((3 < n) && (n < 21))
      return ors[0];
    auto nm10 = n % 10;
    if ((nm10 >= 1) && (nm10 <= 3))
      return ors[nm10];
    return ors[0];
  }

  static const std::vector<std::string> ones {"","one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
  static const std::vector<std::string> teens {"ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen","sixteen", "seventeen", "eighteen", "nineteen"};
  static const std::vector<std::string> tens {"", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};

  const std::string
  num2name (uint64_t num)
  {
    if (num < 10) {
      return ones[num];
    } else if (num < 20) {
      return teens [num - 10];
    } else if (num < 100) {
      return tens[num / 10] + ((num % 10 != 0) ? " " + num2name(num % 10) : "");
    } else if (num < 1000) {
      return num2name(num / 100) + " hundred" + ((num % 100 != 0) ? " " +
                                                 num2name(num % 100) : "");
    } else if (num < 1000000) {
      return num2name(num / 1000) + " thousand" + ((num % 1000 != 0) ? " " +
                                                   num2name(num % 1000) : "");
    } else if (num < 1000000000) {
      return num2name(num / 1000000) + " million" + ((num % 1000000 != 0) ? " " +
                                                     num2name(num % 1000000) : "");
    } else if (num < 1000000000000) {
      return num2name(num / 1000000000) + " billion" + ((num % 1000000000 != 0) ? " " +
                                                        num2name(num % 1000000000) : "");
    }
    return "error";
  }

  class ConvertStandardExpanded : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

  class Rounding : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

  class OperateBy10 : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

  class Ordinal : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

}

Maths::Decimal::Decimal():
  GenMod(__CLASS_NAME__)
{
  MAKE_CONTROLLER(ConvertStandardExpanded);
  MAKE_CONTROLLER(Rounding);
  MAKE_CONTROLLER(OperateBy10);
  MAKE_CONTROLLER(Ordinal);
}

void
ConvertStandardExpanded::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  /* init */
  auto min_digits = config["min_digits"].get<uint32_t>();
  auto max_digits = config["max_digits"].get<uint32_t>();
  std::string expr{"$"};

  /* structures */
  ret["textinput"] = nlohmann::json::array();
  ret["textinput"][0] = nlohmann::json::array();
  ret["correct_ans"] = nlohmann::json::array();
  ret["correct_ans"][0] = nlohmann::json::array();
  ret["text"] = "Write following expression as a **decimal number**?";

  /* genrate operand as integer formed as maxdigit.mindigit
   */
  auto fractional_digits = rand->get_range(min_digits, min_digits<<1);
  auto whole_digits = rand->get_range(min_digits, max_digits);

  /* */
  uint32_t op1 = 0;
  for (auto ui1=whole_digits; ui1; ui1--) {
      auto f = rand->get_digits_no0(0, 1);
      auto d = rand->get_digits_no0(0, 1);
      op1 += f*d;
      expr += std::to_string(f) + " \\times " + std::to_string(d);
      if (ui1 > 1) expr += " + ";
  }
  /* set op1 for these factors */
  std::string fraction{"."};
  for (auto ui1=0; ui1<fractional_digits; ui1++) {
    auto d = rand->get_digits_no0(0, 1) % 10;
    fraction += std::to_string(d);
    expr += " + " + std::to_string(d) + " \\times 0.";
    for (auto ui2=0; ui2<ui1; ui2++) expr+="0";
    expr += "1";
  }

  /* */
  expr += " =$";
  ret["textinput"][0][0] = expr;
  ret["textinput"][0][1] = "_";
  ret["correct_ans"][0][0] = nullptr;
  ret["correct_ans"][0][1] = std::to_string(op1) + fraction;
}

void
Rounding::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  /* init */
  auto min_digits = config["min_digits"].get<uint32_t>();
  auto max_digits = config["max_digits"].get<uint32_t>();
  auto rounding = config["rounding"].get<std::string>(); /* trunc / near */
  auto roundto = config["roundto"][rand->get() % config["roundto"].size()].get<float>();

  /* structures */
  ret["textinput"] = nlohmann::json::array();
  ret["textinput"][0] = nlohmann::json::array();
  ret["correct_ans"] = nlohmann::json::array();
  ret["correct_ans"][0] = nlohmann::json::array();

  /* genrate operand as integer formed as maxdigit.mindigit
   */
  float fnum = rand->get_digits_no0(min_digits+max_digits, min_digits+max_digits), fnans;
  fnum /= 13; /* divide by prime to get a floting point number */
  {
    for (uint32_t ui=0; ui<min_digits; ui++) fnum *= 10;
    fnum = (uint32_t) fnum;
    for (uint32_t ui=0; ui<min_digits; ui++) fnum /= 10;
  }

  /* operate */
  if ("near" == rounding) {
    fnans = fnum + (roundto / 2);
    fnans = ((uint32_t)(fnans / roundto))*roundto;
  }

  /* */
  {
    std::stringstream ss;
    ss << "Round $" << fnum << "$ to the nearest $";
    if (min_digits)
      ss << std::fixed << std::setprecision(2) << roundto << "$";
    else
      ss << ((uint32_t) roundto) << "$";
    ret["text"] = ss.str();
    ss.str(std::string());
    if (min_digits)
      ss << std::fixed << std::setprecision(2) << fnans;
    else
      ss << ((uint32_t) fnans);
    ret["textinput"][0][0] = "_";
    ret["correct_ans"][0][0] = ss.str();
  }
}

void
OperateBy10::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  /* init */
  auto min_digits = config["min_digits"].get<uint32_t>();
  auto max_digits = config["max_digits"].get<uint32_t>();
  uint32_t op1 = rand->get_digits_no0(max_digits, max_digits);
  uint32_t op2 = rand->get_digits_no0(min_digits, min_digits);

  /* choose op */
  std::string op;
  do {
    auto i = rand->get() % config["op_types"].size();
    op = config["op_types"][i].get<std::string>();
  } while (false);

  /* choose what to multiply with */
  uint32_t mult = rand->get_range(1, min_digits+max_digits-1);
  uint32_t aop1{op1}, aop2{op2};
  for (uint32_t ui=mult; ui; ui--) {
    if ("*" == op) {
      aop1 *= 10;
      aop1 += aop2 % 10;
      aop2 /= 10;
    } else {
      aop2 *= 10;
      aop2 += aop1 % 10;
      aop1 /= 10;
    }
  }

  /* structures */
  ret["textinput"] = nlohmann::json::array();
  ret["textinput"][0] = nlohmann::json::array();
  ret["textinput"][0][0] = "_";
  ret["correct_ans"] = nlohmann::json::array();
  ret["correct_ans"][0] = nlohmann::json::array();
  {
    std::stringstream ss;
    ss << "Compute: $" << op1 << ".";
    reversess(ss, std::to_string(op2));
    for (uint32_t ui=mult; ui; ui--)
      ss << (("*" == op) ? " \\times " : " \\div ") << 10;
    ss << "$";
    ret["text"] = ss.str();
    ss.str(std::string());
    ss << aop1 << ".";
    if ("*" == op) {
      ss << aop2;
    } else {
      reversess(ss, std::to_string(aop2));
    }
    ret["correct_ans"][0][0] = ss.str();
  }
}

void
Ordinal::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  /* init */
  auto maxval = config["maxval"].get<uint32_t>();
  uint32_t op1 = rand->get_range(0, maxval);

  /* structures */
  ret["textinput"] = nlohmann::json::array();
  ret["textinput"][0] = nlohmann::json::array();
  ret["textinput"][0][0] = "_";
  ret["correct_ans"] = nlohmann::json::array();
  ret["correct_ans"][0] = nlohmann::json::array();
  {
    std::stringstream ss;
    ss << "What is ordinal of: $" << op1 << "$ ?";
    ret["text"] = ss.str();
    ss.str(std::string());
    ss << op1 << getOrdinal(op1);
    ret["correct_ans"][0][0] = ss.str();
  }
}

/* create global object */
namespace {
  Maths::Decimal a{};
}
