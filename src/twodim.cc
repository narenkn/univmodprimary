#include <random>
#include <sstream>
#include <set>
#include "nlohmann/json.hpp"
#ifdef SABUILD
#include "sa/genmain.h"
#else
# ifdef WASMBUILD
#include "src/genmain.h"
# else
#include "top/genmain.h"
# endif
#endif
#include "twodim.h"
#include <boost/algorithm/string/join.hpp>

namespace {

  class AreaPerimeter : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

}

namespace {
  /* implementation of triangle formula as in https://en.wikipedia.org/wiki/Shoelace_formula
     Given points xy0,xy1,xy2,xy3,xy4,xy5 as vector x0,y0,x1,y1,x2,y2,x3,y3,x4,y5,x5,y5
   */
  uint32_t
  polygon_area(std::vector<uint32_t>& dims)
  {
    int32_t a{0};
    if (dims.size() & 1) return 0;
    for (uint32_t ui=0; ui<dims.size(); ui+=2) {
      uint32_t ui1 = (ui+3) < dims.size() ? ui+2 : 0;
      a += int32_t(dims[ui]*dims[ui1+1]) - int32_t(dims[ui+1]*dims[ui1]);
    }
    return std::abs(int32_t(a/2));
  }
  uint32_t
  polygon_perimeter(std::vector<uint32_t>& dims)
  {
    uint32_t p{0};
    if (dims.size() & 1) return p;
    for (uint32_t ui=0; ui<dims.size(); ui++) {
      p += std::abs(int32_t(dims[ui]-dims[((ui+2)<dims.size()) ? ui+2 : ui&1]));
    }
    return p;
  }
}

Maths::Twodim::Twodim():
  GenMod(__CLASS_NAME__)
{
  MAKE_CONTROLLER(AreaPerimeter);
}

void
AreaPerimeter::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  /* init */
  auto test_type = config["test_type"].get<std::string>();
  auto ticks = config["ticks"].get<uint32_t>();
  auto num_edges = config["num_edges"].get<uint32_t>();
  auto units = config["units"].get<std::string>();

  /* */
  ret["datastr"] = nlohmann::json::object();
  ret["datastr"]["xticks"] = ticks;
  ret["datastr"]["yticks"] = ticks;
  ret["datastr"]["width"] = 640;
  ret["datastr"]["height"] = 640;
  ret["datastr"]["imgsz"] = 50;
  ret["datastr"]["candot"] = false;
  ret["datastr"]["os"] = nlohmann::json::array();

  /* structures */
  ret["text"] = "Find the " + test_type + " of the shaded region?";
  ret["textinput"] = nlohmann::json::array();
  ret["correct_ans"] = nlohmann::json::array();

  /* */
  for (uint32_t objid=0; objid<1; objid++) {
    ret["datastr"]["os"].push_back(nlohmann::json::object());
    ret["datastr"]["os"].back()["type"] = "polygon";
    ret["datastr"]["os"].back()["color"] = "silver";
    ret["datastr"]["os"].back()["v"] = nlohmann::json::array();
    std::vector<uint32_t> dims, dimsa;
    do {
      dims.clear();
      for (uint32_t ui=num_edges-1; ui; ui--) {
        auto v = rand->get() % ticks;
        dims.push_back(v);
      }
      std::set<int> s( dims.begin(), dims.end() );
      dims.assign( s.begin(), s.end() );
      // sort(dims.begin(), dims.end());
    } while ((dims.size() < 5) or (0 == (dims.size()&1)));
    dims.push_back(dims[0]);
    /* */
    for (uint32_t idx=0; idx<dims.size();idx++) {
      ret["datastr"]["os"].back()["v"].push_back(nlohmann::json::array());
      dimsa.push_back(dims[idx&(~1)]);
      dimsa.push_back(dims[idx?((idx&1)?idx:idx-1):idx]);
      ret["datastr"]["os"].back()["v"].back()[0] = dims[idx&(~1)];
      ret["datastr"]["os"].back()["v"].back()[1] = dims[idx?((idx&1)?idx:idx-1):idx];
    }
    /* */
    auto ans = ("area" == test_type) ? polygon_area(dimsa) : polygon_perimeter(dims);
    ret["textinput"].push_back(nlohmann::json::array());
    ret["correct_ans"].push_back(nlohmann::json::array());
    ret["textinput"].back().push_back(test_type+std::to_string(objid)+":");
    ret["textinput"].back().push_back("_");
    ret["correct_ans"].back().push_back(nullptr);
    ret["correct_ans"].back().push_back(std::to_string(ans));
  }
}

/* create global object */
namespace {
  Maths::Twodim a{};
}
