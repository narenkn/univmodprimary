#include <random>
#include <sstream>
#include "nlohmann/json.hpp"
#ifdef SABUILD
#include "sa/genmain.h"
#else
# ifdef WASMBUILD
#include "src/genmain.h"
# else
#include "top/genmain.h"
# endif
#endif
#include "currency.h"
#include <boost/algorithm/string/join.hpp>

namespace {

  class Coins : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans);
  };

}

Maths::Currency::Currency():
  GenMod(__CLASS_NAME__)
{
  MAKE_CONTROLLER(Coins);
}

void
Coins::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  /* config */
  auto test_type = config["test_type"].get<std::string>();
  std::vector<std::uint32_t> denom_pence, denom_pound;
  auto pound2pence = config["pound2pence"].get<uint32_t>();
  for (uint32_t ui=config["denom_pence"].size(); ui; ui--) {
    denom_pence.push_back(config["denom_pence"][ui-1].get<uint32_t>());
  }
  for (uint32_t ui=config["denom_pound"].size(); ui; ui--) {
    denom_pound.push_back(config["denom_pound"][ui-1].get<uint32_t>());
  }
  auto pencename = config["pencename"].get<std::string>();
  auto poundname = config["poundname"].get<std::string>();
  auto pencestr = config["pencestr"].get<std::string>();
  auto poundstr = config["poundstr"].get<std::string>();

  /* structures */
  ret["textinput"] = nlohmann::json::array();
  ret["correct_ans"] = nlohmann::json::array();

  /* */
  auto minc = config["min_coins"].get<uint32_t>();
  auto maxc = config["max_coins"].get<uint32_t>();
  if (("howmanycoins" == test_type) or ("howmanymorepence" == test_type)) {
    uint32_t totpence{0}, totpounds{0};
    std::stringstream ss;
    ss << (("howmanymorepence" == test_type) ? "If I have: " : "How much is: ");
    for (auto &dp: denom_pence) {
      auto n = rand->get_range(minc, maxc);
      if (n > 0) {
        totpence += dp * n;
        ss << n << " coins of " << dp << " " << pencename << ", ";
      }
    }
    for (auto &dp: denom_pound) {
      auto n = rand->get_range(minc, maxc);
      if (n > 0) {
        totpounds += dp * n;
        ss << n << " bills of " << dp << " " << poundname << ", ";
      }
    }
    ss << (("howmanymorepence" == test_type) ? "how many pence do I need to reach the next pound?" : "altogether?");
    ret["text"] = ss.str();
    totpounds += totpence / pound2pence;
    totpence %= pound2pence;
    if ("howmanymorepence" == test_type) totpence = pound2pence - totpence;
    /* */
    if ("howmanycoins" == test_type) {
      ret["textinput"].push_back(nlohmann::json::array());
      ret["correct_ans"].push_back(nlohmann::json::array());
      ret["textinput"].back().push_back(poundstr + ": ");
      ret["textinput"].back().push_back("_");
      ret["correct_ans"].back().push_back(nullptr);
      ret["correct_ans"].back().push_back(std::to_string(totpounds));
    }
    ret["textinput"].push_back(nlohmann::json::array());
    ret["correct_ans"].push_back(nlohmann::json::array());
    ret["textinput"].back().push_back(pencestr + ": ");
    ret["textinput"].back().push_back("_");
    ret["correct_ans"].back().push_back(nullptr);
    ret["correct_ans"].back().push_back(std::to_string(totpence));
  } else if ("makeamount" == test_type) {
    auto numc = config["num_coins"].get<uint32_t>();
    uint32_t totpence{0}, totpounds{0}, count{0};

    ret["check_help"] = nlohmann::json::object();
    ret["check_help"]["pences"] = nlohmann::json::array();
    ret["check_help"]["pounds"] = nlohmann::json::array();

    for (auto &dp: denom_pence) {
      if (count > numc) break;
      count ++;
      auto n = rand->get_range(minc, maxc);
      if (n > 0) {
        totpence += dp * n;
        ret["textinput"].push_back(nlohmann::json::array());
        ret["correct_ans"].push_back(nlohmann::json::array());
        ret["textinput"].back().push_back(pencestr + std::to_string(dp) + ": ");
        ret["textinput"].back().push_back("_");
        ret["correct_ans"].back().push_back(nullptr);
        ret["correct_ans"].back().push_back(std::to_string(n));
        ret["check_help"]["pences"].push_back(dp);
      }
    }
    count = 0;
    for (auto &dp: denom_pound) {
      if (count > numc) break;
      count ++;
      auto n = rand->get_range(minc, maxc);
      if (n > 0) {
        totpounds += dp * n;
        ret["textinput"][0].push_back(poundstr + std::to_string(dp) + ": ");
        ret["textinput"][0].push_back("_");
        ret["correct_ans"][0].push_back(nullptr);
        ret["correct_ans"][0].push_back(std::to_string(n));
        ret["check_help"]["pounds"].push_back(dp);
      }
    }
    totpounds += totpence / pound2pence;
    totpence %= pound2pence;
    ret["check_help"]["totpounds"] = totpounds;
    ret["check_help"]["totpence"] = totpence;
    ret["text"] = "How many denonimations you need to make a total of " + poundstr + " " +
      std::to_string(totpounds) + "." + std::to_string(totpence) + "?";
  }
}

void
Coins::check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans)
{
  auto test_type = config["test_type"].get<std::string>();
  if (("howmanycoins" == test_type) or ("howmanymorepence" == test_type)) {
    return MathsChecks::check_multiarray(ret, config, question, ans);
  }

  if ("makeamount" == test_type) {
    auto pound2pence = config["pound2pence"].get<uint32_t>();
    uint32_t totpence{0}, totpounds{0};
    if (config["num_pence"].is_array()) {
      for (uint32_t ui=config["num_pence"].size(); ui<=question["check_help"]["pences"].size();
           ui--) {
        totpence += config["num_pence"][ui-1].get<uint32_t>() *
          question["check_help"]["pences"][ui-1].get<uint32_t>();
      }
    }

    if (config["num_pound"].is_array()) {
      for (uint32_t ui=config["num_pound"].size(); ui<=question["check_help"]["pounds"].size();
           ui--) {
        totpounds += config["num_pound"][ui-1].get<uint32_t>() *
          question["check_help"]["pounds"][ui-1].get<uint32_t>();
      }
    }

    /* compute & check */
    totpounds += totpence / pound2pence;
    totpence %= pound2pence;
    auto exp_totpounds = ret["check_help"]["totpounds"].get<uint32_t>();
    auto exp_totpence = ret["check_help"]["totpence"].get<uint32_t>();
    ret["result"] = (totpounds == exp_totpounds) and (totpence == exp_totpence);
    return;
  }

  ret["result"] = false;
}

/* create global object */
namespace {
  Maths::Currency a{};
}
