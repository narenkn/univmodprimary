#include <random>
#include <sstream>
#include "nlohmann/json.hpp"
#ifdef SABUILD
#include "sa/genmain.h"
#else
# ifdef WASMBUILD
#include "src/genmain.h"
# else
#include "top/genmain.h"
# endif
#endif
#include "percent.h"
#include <algorithm>
#include <sstream>

namespace {

  /* pcent_op2 % op1 = op2
   */
  uint32_t
  gen_pcent_ops( std::shared_ptr<RandGenerator> rand,
                 uint32_t digits, uint32_t& op1, uint32_t& op2, bool pcent_lt100=false ) {
    uint32_t pcent;
  redo_gen_pcent_ops:
    op1 = rand->get_digits_no0(digits, digits);
    for (pcent = rand->get_digits_no0(digits-1, digits-1); (pcent*op1)%100; pcent++);
    op2 = (op1*pcent) / 100;
    if (pcent_lt100 && (op2 >= op1)) goto redo_gen_pcent_ops;
    else if (op2 == op1) goto redo_gen_pcent_ops;
    return pcent;
  }

  class Number : public Controller {
  public:
    void gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config);
    void check(nlohmann::json& ret, nlohmann::json& config, nlohmann::json& question, nlohmann::json& ans) { return MathsChecks::check_multiarray(ret, config, question, ans); }
  };

}

Maths::Percent::Percent():
  GenMod(__CLASS_NAME__)
{
  MAKE_CONTROLLER(Number);
}

void
Number::gen(nlohmann::json& ret, std::shared_ptr<RandGenerator> rand, nlohmann::json& config)
{
  /* init */
  auto digits = config["digits"].get<uint32_t>();
  auto num_vars = config["num_vars"].get<uint32_t>();
  auto test_type = config["test_type"].get<std::string>();

  /* pcent * op1 = op2;
   */
  uint32_t op1, op2;
  auto pcent = gen_pcent_ops(rand, digits, op1, op2, "illustrate" == test_type);

  /* */
  if ("ask" == test_type) {
    ret["text"] = "What is " + std::to_string(pcent) + "% of " + std::to_string(op1) + "?";
  } else {
    ret["text"] = "";
  }
  ret["textinput"] = nlohmann::json::array();
  ret["textinput"][0] = nlohmann::json::array();
  ret["textinput"][0][0] = "_";
  ret["correct_ans"] = nlohmann::json::array();
  ret["correct_ans"][0] = nlohmann::json::array();
  if ("illustrate" == test_type) {
    ret["datastr"] = nlohmann::json::object();
    ret["datastr"]["size"] = uint32_t(sqrt(op1)+1);
    ret["datastr"]["fill"] = nlohmann::json::array();
    for (uint32_t i=op1, k=0; i; i--, k++)
      ret["datastr"]["fill"].push_back(bool(k < op2));
    ret["textinput"][0][1] = "$\\%$";
    ret["correct_ans"][0][0] = std::to_string(pcent);
    ret["correct_ans"][0][1] = nullptr;
  } else if ("illustrate-fraction" == test_type) {
    auto op1_sq = uint32_t(sqrt(op1)+1);
    ret["datastr"] = nlohmann::json::object();
    ret["datastr"]["size"] = op1_sq;
    ret["datastr"]["fill"] = nlohmann::json::array();
    for (uint32_t i=op1_sq*op1_sq, k=0; i; i--, k++)
      ret["datastr"]["fill"].push_back(bool(k < op2));
    ret["textinput"][0][0] = "N = ";
    ret["textinput"][0][1] = "_";
    ret["textinput"][0][2] = ", D = ";
    ret["textinput"][0][3] = "_";
    ret["correct_ans"][0][0] = nullptr;
    ret["correct_ans"][0][1] = std::to_string(op2);
    ret["correct_ans"][0][2] = nullptr;
    ret["correct_ans"][0][3] = std::to_string(op1_sq*op1_sq);
  } else {
    ret["correct_ans"][0][0] = std::to_string(op2);
  }
}

/* create global object */
namespace {
  Maths::Percent a{};
}
